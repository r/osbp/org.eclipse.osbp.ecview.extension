/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.presentation.clientside.client;

import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.vaadin.client.renderers.Renderer;
import com.vaadin.client.widget.grid.RendererCellReference;

/**
 * The Class PriceRenderer.
 */
public class PriceRenderer implements Renderer<String> {
	
	/* (non-Javadoc)
	 * @see com.vaadin.client.renderers.Renderer#render(com.vaadin.client.widget.grid.RendererCellReference, java.lang.Object)
	 */
	@Override
	public void render(RendererCellReference cell, String htmlString) {
		cell.getElement().setInnerSafeHtml(
				SafeHtmlUtils.fromSafeConstant(htmlString));
	}
}