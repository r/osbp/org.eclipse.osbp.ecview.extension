/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.presentation.clientside.client;

import com.vaadin.client.connectors.AbstractRendererConnector;
import com.vaadin.shared.ui.Connect;

/**
 * The Class QuantityRendererConnector.
 */
@SuppressWarnings({ "restriction", "serial" })
@Connect(org.eclipse.osbp.ecview.extension.grid.presentation.renderer.QuantityRenderer.class)
public class QuantityRendererConnector extends AbstractRendererConnector<String> {
	
	/* (non-Javadoc)
	 * @see com.vaadin.client.connectors.AbstractRendererConnector#getRenderer()
	 */
	@Override
	public QuantityRenderer getRenderer() {
		return (QuantityRenderer) super.getRenderer();
	}
}
