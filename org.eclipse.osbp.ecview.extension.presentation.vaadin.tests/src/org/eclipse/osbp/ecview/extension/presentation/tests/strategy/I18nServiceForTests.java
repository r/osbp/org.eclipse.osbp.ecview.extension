/**
 *                                                                            
 *  Copyright (c) 2015 - 2016 - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Loetz GmbH&Co.KG - Initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.presentation.tests.strategy;

import java.util.Locale;

import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;
import org.eclipse.osbp.runtime.common.i18n.II18nService;

/**
 * A test I18n service.
 */
public class I18nServiceForTests extends AbstractDisposable implements
		II18nService {

	public static final String KEY__NAME = "tests.name";
	public static final String KEY__AGE = "tests.age";
	public static final String KEY__VALUE = "tests.value";

	@Override
	public String getValue(String i18nKey, Locale locale) {

		if (locale == Locale.GERMAN) {
			return getGerman(i18nKey);
		} else if (locale == Locale.ENGLISH) {
			return getEnglish(i18nKey);
		}

		return getEnglish(i18nKey);
	}

	private String getEnglish(String i18nKey) {
		if (i18nKey.equals(KEY__NAME)) {
			return "Name";
		} else if (i18nKey.equals(KEY__AGE)) {
			return "Age";
		} else if (i18nKey.equals(KEY__VALUE)) {
			return "Value";
		} else
			return "";
	}

	private String getGerman(String i18nKey) {
		if (i18nKey.equals(KEY__NAME)) {
			return "Name";
		} else if (i18nKey.equals(KEY__AGE)) {
			return "Alter";
		} else if (i18nKey.equals(KEY__VALUE)) {
			return "Wert";
		} else
			return "";
	}

	@Override
	protected void internalDispose() {

	}

}
