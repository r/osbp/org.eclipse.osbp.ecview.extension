package org.eclipse.osbp.ecview.extension.strategy;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.ecview.extension.api.IFocusingEnhancer;
import org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.osgi.service.component.annotations.Component;

@Component(property = { "ecview.focusing.enhancer.id=DefaultFocusingEnhancer",
		"ecview.focusing.enhancer.default=true" })
public class DefaultFocusingEnhancer implements IFocusingEnhancer {
	@SuppressWarnings("serial")
	private static final List<String> strategies = new ArrayList<String>() {
		{
			add("cx.enter.forward");
			add("cx.enter.backward");
			add("cx.altenter.forward");
			add("cx.tab.forward");
			add("cx.tab.backward");
		}
	};

	@Override
	public void enhanceFocusing(final YStrategyLayout yLayout) {
		yLayout.getFocusingStrategies().clear();
		for (String strategy : strategies) {
			YDelegatingFocusingStrategy yStgy = YECviewFactory.eINSTANCE.createYDelegatingFocusingStrategy();
			yStgy.setDelegateStrategyId(strategy);
			yLayout.getFocusingStrategies().add(yStgy);
		}
	}
}
