/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import java.util.ArrayList;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCssLayout;

import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;

/**
 * Vertical layouting.
 */
public class CssLayoutLayoutingStrategy extends AbstractLayoutingStrategy {

	/**
	 * Instantiates a new css layout layouting strategy.
	 */
	public CssLayoutLayoutingStrategy() {
		super(null);
	}

//	@Override
//	public void layout(YLayoutingInfo layoutingInfo) {
//		YStrategyLayout yLayout = layoutingInfo.getLayout();
//		layoutingInfo.getActiveSuspectInfos().clear();
//
//		YCssLayout content = (YCssLayout) createContentLayout();
//
//		for (YSuspect suspect : new ArrayList<YSuspect>(
//				yLayout.getSuspects())) {
//			YSuspectInfo suspectInfo = layoutingInfo
//					.createSuspectInfo(suspect);
//			// add the suspectInfo as active suspect
//			layoutingInfo.getActiveSuspectInfos().add(suspectInfo);
//
//			YEmbeddable yEmbeddable = prepareElementForSuspect(suspectInfo);
//
//			// if (ySuspect.getTags().contains(Group.DISCOUNT.groupId)){
//			// cellStyle.addSpanInfo(0,rowId1,0,rowId1);
//			// rowId1++;
//			// } else {
//			// cellStyle.addSpanInfo(1,rowId2,1,rowId2);
//			// rowId2++;
//			// }
//			content.addElement(yEmbeddable);
//		}
//
//		layoutingInfo.setContent(content);
//	}

	/* (non-Javadoc)
 * @see org.eclipse.osbp.ecview.extension.strategy.AbstractLayoutingStrategy#createContentLayout()
 */
@Override
	protected YLayout createContentLayout() {
		return ExtensionModelFactory.eINSTANCE.createYCssLayout();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.strategy.AbstractLayoutingStrategy#addElement(org.eclipse.osbp.ecview.core.common.model.core.YLayout, org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable)
	 */
	@Override
	protected void addElement(YLayout layout, YEmbeddable element) {
		layout.addElement(element);
	}

}
