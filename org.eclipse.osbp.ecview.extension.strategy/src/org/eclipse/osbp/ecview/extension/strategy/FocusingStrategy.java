package org.eclipse.osbp.ecview.extension.strategy;

import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeDefinition;

public class FocusingStrategy extends AbstractFocusingStrategy {

	@Override
	public KeyStrokeDefinition getKeyStrokeDefinition() {
		return null;
	}

	@Override
	int getDirection() {
		return 1;
	}

}
