/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import java.util.ArrayList;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayout;

import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;

/**
 * Vertical layouting.
 */
public class FormLayoutLayoutingStrategy extends AbstractLayoutingStrategy {

	/** The left. */
	private YFormLayout left;
	
	/** The right. */
	private YFormLayout right;
	
	/** The index. */
	private int index;

	/**
	 * The Enum Group.
	 */
	public enum Group {
		
		/** The discount. */
		DISCOUNT("discount"), 
		
		/** The nondiscount. */
		NONDISCOUNT("non-discount"), 
		
		/** The nothing. */
		NOTHING("nothing");

		/** The group id. */
		private final String groupId;

		/**
		 * Instantiates a new group.
		 *
		 * @param groupId
		 *            the group id
		 */
		Group(String groupId) {
			this.groupId = groupId;
		}
	}

	/**
	 * Instantiates a new form layout layouting strategy.
	 */
	public FormLayoutLayoutingStrategy() {
		super(null);
	}

//	@Override
//	public void layout(YLayoutingInfo layoutingInfo) {
//		YStrategyLayout yLayout = layoutingInfo.getLayout();
//		layoutingInfo.getActiveSuspectInfos().clear();
//
//		YHorizontalLayout content = (YHorizontalLayout) createContentLayout();
//
//		for (YSuspect suspect : new ArrayList<YSuspect>(
//				yLayout.getSuspects())) {
//			YSuspectInfo suspectInfo = layoutingInfo
//					.createSuspectInfo(suspect);
//			// add the suspectInfo as active suspect
//			layoutingInfo.getActiveSuspectInfos().add(suspectInfo);
//
//			YEmbeddable yEmbeddable = prepareElementForSuspect(suspectInfo);
//
//			// if (ySuspect.getTags().contains(Group.DISCOUNT.groupId)){
//			// cellStyle.addSpanInfo(0,rowId1,0,rowId1);
//			// rowId1++;
//			// } else {
//			// cellStyle.addSpanInfo(1,rowId2,1,rowId2);
//			// rowId2++;
//			// }
//			addElement(yEmbeddable);
//		}
//
//		layoutingInfo.setContent(content);
//	}

	/* (non-Javadoc)
 * @see org.eclipse.osbp.ecview.extension.strategy.AbstractLayoutingStrategy#createContentLayout()
 */
@Override
	protected YLayout createContentLayout() {
		YHorizontalLayout layout = ExtensionModelFactory.eINSTANCE.createYHorizontalLayout();
		left = ExtensionModelFactory.eINSTANCE.createYFormLayout();
		right = ExtensionModelFactory.eINSTANCE.createYFormLayout();

		layout.addElement(left);
		layout.addElement(right);
		return layout;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.strategy.AbstractLayoutingStrategy#addElement(org.eclipse.osbp.ecview.core.common.model.core.YLayout, org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable)
	 */
	@Override
	protected void addElement(YLayout layout, YEmbeddable element) {
		if(index % 2 == 0) left.addElement(element); else right.addElement(element);
		index++;
	}

}
