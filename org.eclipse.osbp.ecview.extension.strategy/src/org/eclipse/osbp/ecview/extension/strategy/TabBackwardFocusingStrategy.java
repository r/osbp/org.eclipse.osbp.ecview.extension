/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy;

import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeDefinition;
import org.eclipse.osbp.runtime.common.keystroke.ModifierKey;

import com.vaadin.event.ShortcutAction.KeyCode;

/**
 * The Class TabBackwardFocusingStrategy.
 */
public class TabBackwardFocusingStrategy extends AbstractFocusingStrategy {
	public int getDirection() {
		return -1;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.extension.api.IFocusingStrategy#getKeyStrokeDefinition()
	 */
	@Override
	public KeyStrokeDefinition getKeyStrokeDefinition() {
	    int[] modifierKeys = new int[1];
		modifierKeys[0] = ModifierKey.SHIFT;
		KeyStrokeDefinition def = new KeyStrokeDefinition("", KeyCode.TAB, modifierKeys);
		return def;
	}

}