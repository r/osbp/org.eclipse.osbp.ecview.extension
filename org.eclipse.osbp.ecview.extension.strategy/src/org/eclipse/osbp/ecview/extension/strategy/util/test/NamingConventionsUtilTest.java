/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy.util.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import org.eclipse.osbp.ecview.extension.strategy.util.NamingConventionsUtil;

/**
 * The Class NamingConventionsUtilTest.
 */
public class NamingConventionsUtilTest {

	/** The dto fqn. */
	String dtoFqn = "com.osbp.myfirstapp.dtos.AddressDto";
	
	/** The entity fqn. */
	String entityFqn = "com.osbp.myfirstapp.entities.Address";
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test.
	 */
	@Test
	public void test() {
		String toFqnEntity = NamingConventionsUtil.toFqnEntityName(dtoFqn);
		assertEquals(entityFqn, toFqnEntity);
	}

}
