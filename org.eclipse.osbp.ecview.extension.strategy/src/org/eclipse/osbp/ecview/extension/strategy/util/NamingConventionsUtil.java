/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.strategy.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;

/**
 * This util is used to convert between dto packages and entity packages.
 */
public class NamingConventionsUtil {

	/** The Constant pathReplacements. */
	private static final List<Pair> pathReplacements;

	static {
		pathReplacements = new ArrayList<Pair>();
		pathReplacements.add(new Pair(".dtos.", ".entities."));
		pathReplacements.add(new Pair(".dtos", ".entitymodel"));
		pathReplacements.add(new Pair("/dto/", "/entity/"));
		pathReplacements.add(new Pair("/dtos/", "/entities/"));
		pathReplacements.add(new Pair(".dto", ".entity"));
	}

	/**
	 * Replaces package fragments of the Auto-DTOs and returns the package name
	 * used for the Entity.
	 *
	 * @param packageName
	 *            the package name
	 * @return the string
	 */
	public static String toEntityPackage(String packageName) {
		if (packageName == null) {
			return "";
		}
		String result = packageName;
		for (Pair pair : pathReplacements) {
			result = pair.replace(result);
		}
		return result;
	}

	/**
	 * Replaces package fragments of the Auto-DTOs and returns the package name
	 * used for the Entity.
	 *
	 * @param lPackage
	 *            the l package
	 * @return the string
	 */
	public static String toEntityPackage(LTypedPackage lPackage) {
		return toEntityPackage(lPackage.getName());
	}

	/**
	 * Returns the name of the entity for the given type.
	 *
	 * @param lType
	 *            the l type
	 * @return the string
	 */
	public static String toEntityName(LType lType) {
		return (lType instanceof LEnum) ? lType.getName() : toEntityName(lType
				.getName());
	}

	/**
	 * Returns the name of the entity for the given type.
	 *
	 * @param prefix
	 *            the prefix
	 * @return the string
	 */
	public static String toEntityName(String prefix) {
		int idx = prefix.lastIndexOf("Dto");
		return prefix.substring(0, idx);
	}

	/**
	 * Returns the full qualified name of the entity for the given type.
	 *
	 * @param fqnDto
	 *            the fqn dto
	 * @return the string
	 */
	public static String toFqnEntityName(String fqnDto) {
		int idx = fqnDto.lastIndexOf(".");
		String pckgName = fqnDto.substring(0, idx+1);
		String dtoName = fqnDto.substring(idx+1);
		return toEntityPackage(pckgName).concat(toEntityName(dtoName));
	}
	
	/**
	 * Returns the simple name of the entity for the given type.
	 *
	 * @param fqnDto
	 *            the fqn dto
	 * @return the string
	 */
	public static String toSimpleEntityName(String fqnDto) {
		int idx = fqnDto.lastIndexOf(".");
		String dtoName = fqnDto.substring(idx+1);
		return toEntityName(dtoName);
	}
	
	/**
	 * The Class Pair.
	 */
	private static class Pair {
		
		/** The source. */
		private final String source;
		
		/** The target. */
		private final String target;

		/**
		 * Instantiates a new pair.
		 *
		 * @param source
		 *            the source
		 * @param target
		 *            the target
		 */
		public Pair(String source, String target) {
			super();
			this.source = source;
			this.target = target;
		}

		/**
		 * Replace.
		 *
		 * @param value
		 *            the value
		 * @return the string
		 */
		public String replace(String value) {
			return value.replace(source, target);
		}
	}

}
