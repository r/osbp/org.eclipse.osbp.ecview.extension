package org.eclipse.osbp.ecview.extension.strategy;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.osbp.ecview.extension.api.IFocusingStrategy;
import org.osgi.service.component.annotations.Component;

@Component(service = IContextFunction.class, property = "service.context.key=Focusing")
public class FocusingStrategyContextFunction implements IContextFunction {
  @Override
  public Object compute(final IEclipseContext context, final String contextKey) {
    MApplication application = context.get(MApplication.class);
    IEclipseContext appCtx = application.getContext();
    IFocusingStrategy provider = ContextInjectionFactory.make(FocusingStrategy.class, appCtx);
    appCtx.set(IFocusingStrategy.class, provider);
    return provider;
  }
}
