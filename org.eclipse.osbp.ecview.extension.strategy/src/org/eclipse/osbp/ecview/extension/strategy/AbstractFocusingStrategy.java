package org.eclipse.osbp.ecview.extension.strategy;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTab;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTabSheet;
import org.eclipse.osbp.ecview.core.util.emf.ModelUtil;
import org.eclipse.osbp.ecview.extension.api.IFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ui.api.e4.IE4Dialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.UI;

public abstract class AbstractFocusingStrategy implements IFocusingStrategy {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFocusingStrategy.class);

	abstract int getDirection();

	@Override
	public void focus(final Object source, final Object target, final YStrategyLayout yLayout) {
		YView yView = yLayout.getView();
		IViewContext context = ModelUtil.getViewContext(yView);
		IWidgetAssocationsService<Object, ? extends YElement> service = context
				.getService(IWidgetAssocationsService.ID);
		YEmbeddable yCurrentFocus = (YEmbeddable) service.getModelElement(target);
		if(yCurrentFocus == null) {
			LOGGER.debug("{}", "current focus not set");
		}
		YEmbeddable yNextFocus = findElementToFocus(yCurrentFocus, service, true);
		if(yNextFocus != null) {
			yView.setCurrentFocus((YFocusable) yNextFocus);
		} else {
			LOGGER.debug("{}", "next focus not found");
		}
	}

	@Override
	public void focusField(YView yView, String fieldId) {
		IViewContext context = ModelUtil.getViewContext(yView);
		IWidgetAssocationsService<Object, ? extends YElement> service = context.getService(IWidgetAssocationsService.ID);
		YEmbeddable yEmbeddable = null;
		if(fieldId != null) {
			LOGGER.debug("current field id:{}", fieldId);
			yEmbeddable = (YEmbeddable) service.getModelElement(fieldId);
		} else {
			LOGGER.debug("current field id is null");
			yEmbeddable = findElementToFocus((YEmbeddable)service.getModelElement(0), service, false);
		}
		if(yEmbeddable instanceof YTabSheet) {
			YTab tab = ((YTabSheet)yEmbeddable).getTabs().get(0);
			focusField(tab.getView(), null);
		}
		if(yView != null && yEmbeddable instanceof YFocusable) {
			yView.setCurrentFocus((YFocusable)yEmbeddable);
		}
	}
	
	protected YEmbeddable findElementToFocus(YEmbeddable yElement, IWidgetAssocationsService<Object, ? extends YElement> service, boolean focusNext) {
		YEmbeddable nextFocusElement = null;
		int maxIdx = service.getModelElements().size()-1;
		LOGGER.debug("max idx:{}", maxIdx);
		int idx = 0;
		if(getDirection() < 0) {
			idx = maxIdx;
		}
		if (yElement instanceof YFocusable) {
			idx = ((YFocusable) yElement).getLayoutIdx();
			LOGGER.debug("current focus index:{}", idx);
			if(focusNext) {
				idx += getDirection();
			}
		}
		int turnAround = 0;
		boolean found = false;
		do {
			LOGGER.debug("next focus index:{}", idx);
			if(idx < 0) {
				turnAround ++;
				idx = maxIdx;
				LOGGER.debug("goto last index:{} turnaround:{}", idx, turnAround);
			}
			if(idx > maxIdx) {
				turnAround ++;
				idx = 0;
				LOGGER.debug("goto first index:{} turnaround:{}", idx, turnAround);
			}
			nextFocusElement = (YEmbeddable) service.getModelElement(idx);
			if(nextFocusElement != null && !(nextFocusElement instanceof YTabSheet)) {
				LOGGER.debug("potential next focus element:{}", nextFocusElement.getId());
				found = (nextFocusElement instanceof YFocusable);
				LOGGER.debug("potential next focus element is focusable:{}", found);
				if(found && nextFocusElement instanceof YField) {
					found = ((YField) nextFocusElement).isEditable() && ((YField) nextFocusElement).isEnabled() && ((YField) nextFocusElement).isVisible(); 
					LOGGER.debug("potential next focus element is field editable:{}", found);
				} else {
					LOGGER.debug("unknown type");
				}
			}
			idx = idx + getDirection();
		} while(turnAround < 3 && !found);
		return nextFocusElement;
	}
}
