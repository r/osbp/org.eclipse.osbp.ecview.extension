/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.sample;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;

import org.eclipse.osbp.runtime.common.annotations.Dispose;

/**
 * The Class Person.
 */
@SuppressWarnings("all")
public class Person implements Serializable {
	
	/** The property change support. */
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(
			this);

	/** The disposed. */
	@Dispose
	private boolean disposed;

	/** The id. */
	private String id = java.util.UUID.randomUUID().toString();

	/** The name. */
	private String name;

	/** The name2. */
	private String name2;

	/** The age. */
	private int age;

	/** The vitality. */
	private double vitality;

	/** The birthday. */
	private Date birthday;

	/** The native german. */
	private boolean nativeGerman;

	/** The gender. */
	private Gender gender;

	/**
	 * Returns true, if the object is disposed. Disposed means, that it is
	 * prepared for garbage collection and may not be used anymore. Accessing
	 * objects that are already disposed will cause runtime exceptions.
	 *
	 * @return true, if is disposed
	 */
	public boolean isDisposed() {
		return this.disposed;
	}

	/**
	 * Adds the property change listener.
	 *
	 * @param listener
	 *            the listener
	 * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * Adds the property change listener.
	 *
	 * @param propertyName
	 *            the property name
	 * @param listener
	 *            the listener
	 * @see PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final String propertyName,
			final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * Removes the property change listener.
	 *
	 * @param listener
	 *            the listener
	 * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 */
	public void removePropertyChangeListener(
			final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	/**
	 * Removes the property change listener.
	 *
	 * @param propertyName
	 *            the property name
	 * @param listener
	 *            the listener
	 * @see PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void removePropertyChangeListener(final String propertyName,
			final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName,
				listener);
	}

	/**
	 * Fire property change.
	 *
	 * @param propertyName
	 *            the property name
	 * @param oldValue
	 *            the old value
	 * @param newValue
	 *            the new value
	 * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
	 */
	public void firePropertyChange(final String propertyName,
			final Object oldValue, final Object newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue,
				newValue);
	}

	/**
	 * Checks whether the object is disposed.
	 * 
	 * @throws RuntimeException
	 *             if the object is disposed.
	 */
	private void checkDisposed() {
		if (isDisposed()) {
			throw new RuntimeException("Object already disposed: " + this);
		}
	}

	/**
	 * Calling dispose will destroy that instance. The internal state will be
	 * set to 'disposed' and methods of that object must not be used anymore.
	 * Each call will result in runtime exceptions.<br/>
	 * If this object keeps composition containments, these will be disposed
	 * too. So the whole composition containment tree will be disposed on
	 * calling this method.
	 */
	@Dispose
	public void dispose() {
		if (isDisposed()) {
			return;
		}
		firePropertyChange("disposed", this.disposed, this.disposed = true);
	}

	/**
	 * Returns the id property or <code>null</code> if not present.
	 *
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Sets the <code>id</code> property to this instance.
	 * 
	 * @param id
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setId(final String id) {
		firePropertyChange("id", this.id, this.id = id);
	}

	/**
	 * Returns the name property or <code>null</code> if not present.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the <code>name</code> property to this instance.
	 * 
	 * @param name
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setName(final String name) {
		firePropertyChange("name", this.name, this.name = name);
	}

	/**
	 * Returns the name2 property or <code>null</code> if not present.
	 *
	 * @return the name2
	 */
	public String getName2() {
		return this.name2;
	}

	/**
	 * Sets the <code>name2</code> property to this instance.
	 * 
	 * @param name2
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setName2(final String name2) {
		firePropertyChange("name2", this.name2, this.name2 = name2);
	}

	/**
	 * Returns the age property or <code>null</code> if not present.
	 *
	 * @return the age
	 */
	public int getAge() {
		return this.age;
	}

	/**
	 * Sets the <code>age</code> property to this instance.
	 * 
	 * @param age
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setAge(final int age) {
		firePropertyChange("age", this.age, this.age = age);
	}

	/**
	 * Gets the vitality.
	 *
	 * @return the vitality
	 */
	public double getVitality() {
		return vitality;
	}

	/**
	 * Sets the vitality.
	 *
	 * @param vitality
	 *            the new vitality
	 */
	public void setVitality(double vitality) {
		firePropertyChange("vitality", this.vitality, this.vitality = vitality);
	}

	/**
	 * Returns the birthday property or <code>null</code> if not present.
	 *
	 * @return the birthday
	 */
	public Date getBirthday() {
		return this.birthday;
	}

	/**
	 * Checks if is native german.
	 *
	 * @return true, if is native german
	 */
	public boolean isNativeGerman() {
		return nativeGerman;
	}

	/**
	 * Sets the native german.
	 *
	 * @param nativeGerman
	 *            the new native german
	 */
	public void setNativeGerman(boolean nativeGerman) {
		firePropertyChange("nativeGerman", this.nativeGerman,
				this.nativeGerman = nativeGerman);
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender
	 *            the new gender
	 */
	public void setGender(Gender gender) {
		firePropertyChange("gender", this.gender, this.gender = gender);
	}

	/**
	 * Sets the <code>birthday</code> property to this instance.
	 * 
	 * @param birthday
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setBirthday(final Date birthday) {
		firePropertyChange("birthday", this.birthday, this.birthday = birthday);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (this.id == null) {
			if (other.id != null)
				return false;
		} else if (!this.id.equals(other.id))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		return result;
	}

}
