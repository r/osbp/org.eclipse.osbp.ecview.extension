/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.sample;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.osgi.service.component.annotations.Component;

import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;

import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridCellStyleGeneratorEditpart;
import org.eclipse.osbp.ecview.extension.grid.editparts.presentation.IGridCellStyleGeneratorFactory;

/**
 * The Class CellStyleGeneratorFactorty.
 */
@Component(immediate = true)
public class CellStyleGeneratorFactorty implements
		IGridCellStyleGeneratorFactory {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.editparts.presentation.IGridCellStyleGeneratorFactory#isFor(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.extension.grid.editparts.IGridCellStyleGeneratorEditpart)
	 */
	@Override
	public boolean isFor(IViewContext uiContext,
			IGridCellStyleGeneratorEditpart editpart) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.editparts.presentation.IGridCellStyleGeneratorFactory#createCellStyleGenerator(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.extension.grid.editparts.IGridCellStyleGeneratorEditpart)
	 */
	@Override
	public Object createCellStyleGenerator(IViewContext uiContext,
			IGridCellStyleGeneratorEditpart editpart)
			throws IllegalArgumentException {

		CxGridDelegateCellStyleGenerator yGenerator = (CxGridDelegateCellStyleGenerator) editpart
				.getModel();
		if (yGenerator.getDelegateId().equals("sampleGrid")) {
			return new Generator();
		}

		return null;

	}

	/**
	 * The Class Generator.
	 */
	@SuppressWarnings("serial")
	private static class Generator implements CellStyleGenerator {

		/* (non-Javadoc)
		 * @see com.vaadin.ui.Grid.CellStyleGenerator#getStyle(com.vaadin.ui.Grid.CellReference)
		 */
		@Override
		public String getStyle(CellReference cellReference) {
			if (cellReference.getPropertyId().equals("name")) {
				return "osbee-bold";
			}
			return null;
		}
	}

}
