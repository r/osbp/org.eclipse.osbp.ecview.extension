/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.sample;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.osbp.ecview.core.common.memento.IECViewMementoManager;
import org.eclipse.osbp.ecview.core.common.model.core.YMemento;
import org.eclipse.osbp.runtime.common.validation.IStatus;

/**
 * A singleton sample memento manager which provides the same memento to all
 * instances of ECViewGridSampleUI.
 */
public class SampleMementoManager implements IECViewMementoManager {

	/** The instance. */
	public static SampleMementoManager INSTANCE = new SampleMementoManager();

	/** The mementos. */
	private Map<String, YMemento> mementos = new HashMap<String, YMemento>();

	/**
	 * Instantiates a new sample memento manager.
	 */
	private SampleMementoManager() {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.memento.IECViewMementoManager#loadMemento(java.lang.String)
	 */
	@Override
	public Object loadMemento(String mementoId) {
		return mementos.get(mementoId);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.memento.IECViewMementoManager#saveMemento(java.lang.String, java.lang.Object)
	 */
	@Override
	public IStatus saveMemento(String mementoId, Object memento) {
		mementos.put(mementoId, (YMemento) memento);
		return IStatus.OK;
	}

}
