/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.sample;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.osbp.ecview.core.common.context.ContextException;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableValueEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.memento.IECViewMementoManager;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YAlignment;
import org.eclipse.osbp.ecview.core.common.model.core.YDelegateConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCheckBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDateTime;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSlider;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayoutCellStyle;
import org.eclipse.osbp.ecview.core.util.emf.ModelUtil;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridEditpart;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;
import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.common.memento.IMementoManager;
import org.eclipse.osbp.runtime.common.validation.IStatus;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.themes.ValoTheme;

/**
 * The Class ECViewGridSampleUI.
 */
@SuppressWarnings("serial")
@Theme(ValoTheme.THEME_NAME)
@Widgetset("org.eclipse.osbp.ecview.extension.widgetset.CxECViewWidgetset")
@Push
public class ECViewGridSampleUI extends UI {

	/** The view context. */
	@SuppressWarnings("unused")
	private IViewContext viewContext;

	/** The result layout. */
	private VerticalLayout resultLayout;

	/** The cx grid. */
	private CxGrid cxGrid;

	/** The col name. */
	private CxGridColumn colName;

	/** The col name2. */
	private CxGridColumn colName2;

	/** The col age. */
	private CxGridColumn colAge;

	/** The col birthday. */
	private CxGridColumn colBirthday;

	/** The names group cell. */
	private CxGridGroupedCell namesGroupCell;

	/** The header row1. */
	private CxGridHeaderRow headerRow1;

	/** The custom cell age. */
	private CxGridMetaCell customCellAge;

	/** The person group cell. */
	private CxGridGroupedCell personGroupCell;

	/** The header row2. */
	private CxGridHeaderRow headerRow2;

	/** The custom cell birthday. */
	private CxGridMetaCell customCellBirthday;

	/** The fgroup1. */
	private CxGridGroupedCell fgroup1;

	/** The footer row. */
	private CxGridFooterRow footerRow;

	/** The fgroup2. */
	private CxGridGroupedCell fgroup2;

	/** The footer row person info. */
	private CxGridFooterRow footerRowPersonInfo;

	/** The custom cell birthday footer. */
	private CxGridMetaCell customCellBirthdayFooter;

	/** The single selection. */
	private CheckBox singleSelection;

	/** The birthday renderer. */
	private CxGridDateRenderer birthdayRenderer;

	/** The age renderer. */
	private CxGridButtonRenderer ageRenderer;

	/** The col vit. */
	private CxGridColumn colVit;

	/** The vit renderer. */
	private CxGridProgressBarRenderer vitRenderer;

	/** The age renderer click endpoint. */
	private YECViewModelValueBindingEndpoint ageRendererClickEndpoint;

	/** The col native german. */
	private CxGridColumn colNativeGerman;

	/** The col gender. */
	private CxGridColumn colGender;

	/** The y vl. */
	private YVerticalLayout yVl;

	/** The button layout. */
	private GridLayout buttonLayout;

	/** The delegating memento manager. */
	private IMementoManager delegatingMementoManager;

	/** The memento manager. */
	private SampleMementoManager mementoManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.UI#init(com.vaadin.server.VaadinRequest)
	 */
	@Override
	protected void init(VaadinRequest request) {

		BundleContext context = FrameworkUtil.getBundle(getClass())
				.getBundleContext();
		ServiceReference<IMementoManager> ref = context
				.getServiceReference(IMementoManager.class);
		delegatingMementoManager = context.getService(ref);

		mementoManager = new SampleMementoManager();

		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		setContent(layout);
		layout.setStyleName(Reindeer.LAYOUT_BLUE);
		layout.setMargin(true);
		layout.setSpacing(true);

		createResultArea(layout);

		VerticalLayout filler = new VerticalLayout();
		layout.addComponent(filler);
		layout.setExpandRatio(filler, 1.0f);
		filler.setSizeFull();
		filler.setMargin(true);
		filler.setStyleName(Reindeer.LAYOUT_WHITE);

		Label description = new Label();
		filler.addComponent(description);
		filler.setComponentAlignment(description, Alignment.BOTTOM_LEFT);
		description.setContentMode(ContentMode.HTML);

	}

	/**
	 * Creates the result area.
	 *
	 * @param layout
	 *            the layout
	 */
	private void createResultArea(VerticalLayout layout) {

		// VaadinObservables.activateRealm(this);

		resultLayout = new VerticalLayout();
		layout.addComponent(resultLayout);
		YView yView = createViewModel();

		VaadinRenderer renderer = new VaadinRenderer();
		try {
			viewContext = renderer.render(resultLayout, yView,
					createRenderingParams());
		} catch (ContextException e) {
			e.printStackTrace();
		}

		createPersons();

		buttonLayout = new GridLayout();
		buttonLayout.setColumns(2);
		buttonLayout.setSizeFull();
		resultLayout.addComponent(buttonLayout);

		Button b = new Button("show/hide Names-Group");
		b.setDescription("Shows or hides the Names-Group-HeaderRow");
		b.setWidth("250px");
		b.addClickListener(e -> enableHeaderAndFooter());
		buttonLayout.addComponent(b);

		Button b2 = new Button("add/remove columns");
		b2.setWidth("250px");
		b2.addClickListener(e -> addAndRemoveColumns());
		buttonLayout.addComponent(b2);

		Button b3 = new Button("add/remove MetaCells");
		b3.setWidth("250px");
		b3.addClickListener(e -> enableCustomCells());
		buttonLayout.addComponent(b3);

		Button b4 = new Button("editor enabled");
		b4.setWidth("250px");
		b4.addClickListener(e -> enableEditor());
		HorizontalLayout b4Layout = new HorizontalLayout(b4);
		buttonLayout.addComponent(b4Layout);

		Button b5 = new Button("select something");
		b5.setWidth("250px");
		singleSelection = new CheckBox("singleSelection");
		singleSelection.setValue(true);
		singleSelection
				.addValueChangeListener(e -> {
					cxGrid.setSelectionType(singleSelection.getValue() ? YSelectionType.SINGLE
							: YSelectionType.MULTI);
				});

		HorizontalLayout b5Layout = new HorizontalLayout(b5, singleSelection);
		b5Layout.setComponentAlignment(singleSelection, Alignment.MIDDLE_LEFT);
		b5Layout.setSpacing(true);
		b5.addClickListener(e -> switchSelectionMode());
		buttonLayout.addComponent(b5Layout);

		TextField birthdayFormat = new TextField("format of birthday");
		birthdayFormat.setValue("%1$tB %1$te, %1$tY");
		birthdayFormat.addValueChangeListener(e -> {
			birthdayRenderer.setDateFormat(birthdayFormat.getValue());
		});
		buttonLayout.addComponent(birthdayFormat);

		IBindableValueEndpointEditpart ep = ModelUtil.getEditpart(viewContext,
				ageRendererClickEndpoint);
		ep.getObservable()
				.addValueChangeListener(
						e -> {
							CxGridRendererClickEvent cxEvent = (CxGridRendererClickEvent) e
									.getObservableValue().getValue();
							Notification.show("Age renderer clicked: "
									+ cxEvent.getRenderer().getId());
						});

		Button b6 = new Button("filter visible");
		b6.setWidth("250px");
		b6.addClickListener(e -> enableFilter());
		buttonLayout.addComponent(b6);

		Button b7 = new Button("header visible");
		b7.setWidth("250px");
		b7.addClickListener(e -> enableHeader());
		buttonLayout.addComponent(b7);

		Button b8 = new Button("footer visible");
		b8.setWidth("250px");
		b8.addClickListener(e -> enableFooter());
		buttonLayout.addComponent(b8);

		Button b9 = new Button("rerender");
		b9.setWidth("250px");
		b9.addClickListener(e -> rerender());
		buttonLayout.addComponent(b9);

		Button b10 = new Button("new model and memento");
		b10.setDescription("Creates a new CxGrid-Model and renders it. If a memento was already created by reordering columns,... then the memento will become applied before rendering.");
		b10.setWidth("250px");
		b10.addClickListener(e -> {
			if (mementoManager.loadMemento(cxGrid.getMementoId()) == null) {
				Notification.show(
						"No memento available yet. Change something.",
						Notification.Type.ERROR_MESSAGE);
				return;
			}
			newModelAndMemento();

		});
		buttonLayout.addComponent(b10);
	}

	/**
	 * Creates the datamodel.
	 */
	private void createPersons() {
		Person person1 = new Person();
		person1.setName("My first name");
		person1.setName2("<b><i>Another</i></b> name");
		person1.setAge(43);
		person1.setVitality(142);
		person1.setBirthday(new Date());
		person1.setNativeGerman(true);
		person1.setGender(Gender.MALE);
		cxGrid.getCollection().add(person1);

		Person person2 = new Person();
		person2.setName("Other name");
		person2.setName2("<b><i>Foo</i></b> Baa");
		person2.setAge(26);
		person2.setVitality(220);
		person2.setBirthday(new Date());
		person2.setGender(Gender.FEMALE);
		cxGrid.getCollection().add(person2);

		Person person3 = new Person();
		person3.setName("Some value");
		person3.setName2("<b><i>Mustermann</i></b> Peter");
		person3.setAge(33);
		person3.setVitality(85);
		person3.setBirthday(new Date());
		person3.setGender(Gender.FEMALE);
		cxGrid.getCollection().add(person3);
	}

	/**
	 * /**
	 *
	 * @return the object
	 */
	private Object enableHeaderAndFooter() {
		if (cxGrid.getHeaders().contains(headerRow1)) {
			cxGrid.getHeaders().remove(headerRow1);
		} else {
			cxGrid.getHeaders().add(headerRow1);
		}

		if (cxGrid.getFooters().contains(footerRowPersonInfo)) {
			cxGrid.getFooters().remove(footerRowPersonInfo);
		} else {
			cxGrid.getFooters().add(footerRowPersonInfo);
		}

		return null;
	}

	/**
	 * Adds and removes defined columns.
	 *
	 * @return the object
	 */
	private Object addAndRemoveColumns() {
		if (cxGrid.getColumns().contains(colAge)) {
			cxGrid.getColumns().remove(colAge);
			cxGrid.getColumns().remove(colBirthday);
		} else {
			int idx = cxGrid.getColumns().indexOf(colName2);
			cxGrid.getColumns().add(idx + 1, colAge);
			cxGrid.getColumns().add(colBirthday);
		}

		return null;
	}

	/**
	 * /**
	 *
	 * @return the object
	 */
	private Object enableCustomCells() {
		if (!headerRow2.getCustomCells().isEmpty()) {
			headerRow1.getCustomCells().clear();
			headerRow2.getCustomCells().clear();
		} else {
			headerRow1.getCustomCells().add(customCellAge);
			headerRow2.getCustomCells().add(customCellBirthday);
		}

		return null;
	}

	/**
	 * /**
	 *
	 * @return the object
	 */
	private Object enableEditor() {
		cxGrid.setEditorEnabled(!cxGrid.isEditorEnabled());
		return null;
	}

	/**
	 * Switches the selection mode.
	 *
	 * @return the object
	 */
	private Object switchSelectionMode() {
		if (cxGrid.getSelectionType() == YSelectionType.SINGLE) {
			cxGrid.setSelection(cxGrid.getCollection().get(0));
		} else {
			cxGrid.getMultiSelection().addAll(cxGrid.getCollection());
		}
		return null;
	}

	/**
	 * /**
	 *
	 * @return the object
	 */
	private Object enableFilter() {
		cxGrid.setFilteringVisible(!cxGrid.isFilteringVisible());
		return null;
	}

	/**
	 * /**
	 *
	 * @return the object
	 */
	private Object enableHeader() {
		cxGrid.setHeaderVisible(!cxGrid.isHeaderVisible());
		return null;
	}

	/**
	 * /**
	 *
	 * @return the object
	 */
	private Object enableFooter() {
		cxGrid.setFooterVisible(!cxGrid.isFooterVisible());
		return null;
	}

	/**
	 * Rerender the model.
	 *
	 * @return the object
	 */
	private Object rerender() {

		IGridEditpart gEp = ModelUtil.findEditpart(cxGrid);
		gEp.requestUnrender();
		gEp.requestRender();

		return null;
	}

	/**
	 * Creates a new model and applies the memento if available.
	 */
	private void newModelAndMemento() {
		yVl.getElements().clear();

		yVl.getElements().add(createCxGrid());
		createPersons();
	}

	/**
	 * Creates the rendering parameters.
	 *
	 * @return the map
	 */
	private Map<String, Object> createRenderingParams() {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> services = new HashMap<String, Object>();
		params.put(IViewContext.PARAM_SERVICES, services);
		services.put(II18nService.class.getName(), new I18nService());
		services.put(IECViewMementoManager.class.getName(), mementoManager);

		return params;
	}

	/**
	 * Creates the view model.
	 *
	 * @return the y view
	 */
	private YView createViewModel() {
		YView yView = CoreModelFactory.eINSTANCE.createYView();

		yVl = ExtensionModelFactory.eINSTANCE.createYVerticalLayout();
		yView.setContent(yVl);

		// creates the cxGrid
		createCxGrid();

		// add the grid
		yVl.addElement(cxGrid);

		return yView;
	}

	/**
	 * Creates the cx grid.
	 *
	 * @return the cx grid
	 */
	protected CxGrid createCxGrid() {
		CxGridFactory fact = CxGridFactory.eINSTANCE;
		cxGrid = fact.createCxGrid();
		cxGrid.setId("sample.grid");
		cxGrid.setMementoEnabled(true);
		cxGrid.setFooterVisible(true);
		cxGrid.setEditorEnabled(true);
		cxGrid.setType(Person.class);
		cxGrid.setFilteringVisible(true);
		cxGrid.setSelectionType(YSelectionType.SINGLE);
		cxGrid.setTypeQualifiedName(Person.class.getName());

		CxGridDelegateCellStyleGenerator cxGenerator = CxGridFactory.eINSTANCE
				.createCxGridDelegateCellStyleGenerator();
		cxGenerator.setDelegateId("sampleGrid");
		cxGrid.setCellStyleGenerator(cxGenerator);

		YVerticalLayoutCellStyle cellStyle = yVl.addCellStyle(cxGrid);
		cellStyle.setAlignment(YAlignment.TOP_FILL);

		colName = fact.createCxGridColumn();
		colName.setPropertyId("name");
		colName.setPropertyPath("name");
		colName.setEditable(true);
		cxGrid.getColumns().add(colName);

		colName2 = fact.createCxGridColumn();
		colName2.setPropertyId("name2");
		colName2.setPropertyPath("name2");
		colName2.setEditable(true);
		colName2.setRenderer(CxGridRendererFactory.eINSTANCE
				.createCxGridHtmlRenderer());

		YTextSearchField name2SF = ExtensionModelFactory.eINSTANCE
				.createYTextSearchField();
		name2SF.setPropertyPath("name2");
		colName2.setSearchField(name2SF);
		cxGrid.getColumns().add(colName2);

		colAge = fact.createCxGridColumn();
		colAge.setPropertyId("age");
		colAge.setPropertyPath("age");
		colAge.setEditable(true);
		ageRenderer = CxGridRendererFactory.eINSTANCE
				.createCxGridButtonRenderer();
		ageRendererClickEndpoint = ageRenderer.createLastClickEventEndpoint();
		colAge.setRenderer(ageRenderer);
		YNumericSearchField ageSF = ExtensionModelFactory.eINSTANCE
				.createYNumericSearchField();
		ageSF.setPropertyPath("age");
		ageSF.setType(Integer.class);
		colAge.setSearchField(ageSF);
		cxGrid.getColumns().add(colAge);

		colNativeGerman = fact.createCxGridColumn();
		colNativeGerman.setPropertyId("nativeGerman");
		colNativeGerman.setPropertyPath("nativeGerman");
		colNativeGerman.setEditable(true);
		YBooleanSearchField nativeGermanSF = ExtensionModelFactory.eINSTANCE
				.createYBooleanSearchField();
		nativeGermanSF.setPropertyPath("nativeGerman");
		colNativeGerman.setSearchField(nativeGermanSF);
		colNativeGerman.setRenderer(CxGridRendererFactory.eINSTANCE
				.createCxGridBooleanRenderer());
		cxGrid.getColumns().add(colNativeGerman);

		colGender = fact.createCxGridColumn();
		colGender.setPropertyId("gender");
		colGender.setPropertyPath("gender");
		colGender.setEditable(true);
		colGender.setRenderer(CxGridRendererFactory.eINSTANCE
				.createCxGridImageRenderer());
		YDelegateConverter yGenderConverter = CoreModelFactory.eINSTANCE
				.createYDelegateConverter();
		yGenderConverter.setConverterId("genderToImageConverter");
		colGender.setConverter(yGenderConverter);
		cxGrid.getColumns().add(colGender);

		colVit = fact.createCxGridColumn();
		colVit.setPropertyId("vitality");
		colVit.setPropertyPath("vitality");
		colVit.setEditable(true);
		vitRenderer = CxGridRendererFactory.eINSTANCE
				.createCxGridProgressBarRenderer();
		vitRenderer.setMaxValue(250);
		colVit.setRenderer(vitRenderer);

		YSlider vitEditor = ExtensionModelFactory.eINSTANCE.createYSlider();
		vitEditor.setMaxValue(250);
		vitEditor.setMinValue(0);
		vitEditor.setResolution(0);
		colVit.setEditorField(vitEditor);

		cxGrid.getColumns().add(colVit);

		colBirthday = fact.createCxGridColumn();
		colBirthday.setPropertyId("birthday");
		colBirthday.setPropertyPath("birthday");
		colBirthday.setEditable(true);
		YDateTime birthdayEditor = ExtensionModelFactory.eINSTANCE
				.createYDateTime();
		colBirthday.setEditorField(birthdayEditor);
		birthdayRenderer = CxGridRendererFactory.eINSTANCE
				.createCxGridDateRenderer();
		birthdayRenderer.setDateFormat("%1$tB %1$te, %1$tY");
		colBirthday.setRenderer(birthdayRenderer);
		cxGrid.getColumns().add(colBirthday);

		//
		// create grouped header
		//

		// group name and name2 in header "Names Group"
		//
		namesGroupCell = fact.createCxGridGroupedCell();
		namesGroupCell.setLabel("Names Group");
		namesGroupCell.getGroupables().add(colName);
		namesGroupCell.getGroupables().add(colName2);

		headerRow1 = fact.createCxGridHeaderRow();
		headerRow1.getGroupings().add(namesGroupCell);
		cxGrid.getHeaders().add(headerRow1);

		customCellAge = fact.createCxGridMetaCell();
		YCheckBox cellAgeCheckbox = ExtensionModelFactory.eINSTANCE
				.createYCheckBox();
		cellAgeCheckbox.setLabel("checkbox in header");
		customCellAge.setElement(cellAgeCheckbox);
		customCellAge.setTarget(colAge);
		headerRow1.getCustomCells().add(customCellAge);

		// group "Names Group" and age in "Person Group"
		//
		personGroupCell = fact.createCxGridGroupedCell();
		personGroupCell.setLabel("Person Group");
		personGroupCell.getGroupables().add(namesGroupCell);
		personGroupCell.getGroupables().add(colAge);

		headerRow2 = fact.createCxGridHeaderRow();
		headerRow2.getGroupings().add(personGroupCell);
		cxGrid.getHeaders().add(headerRow2);

		customCellBirthday = fact.createCxGridMetaCell();
		YNumericField cellBirthdayField = ExtensionModelFactory.eINSTANCE
				.createYNumericField();
		cellBirthdayField.setLabel("NumericField in header");
		customCellBirthday.setElement(cellBirthdayField);
		customCellBirthday.setTarget(colBirthday);
		headerRow2.getCustomCells().add(customCellBirthday);

		//
		// create grouped footer
		//

		// group name and name2 in header "Names Group"
		//
		fgroup1 = fact.createCxGridGroupedCell();
		fgroup1.setLabel("Names Group");
		fgroup1.getGroupables().add(colName);
		fgroup1.getGroupables().add(colName2);

		footerRow = fact.createCxGridFooterRow();
		footerRow.getGroupings().add(fgroup1);
		cxGrid.getFooters().add(footerRow);

		// group "Names Group" and age in "Person Group"
		//
		fgroup2 = fact.createCxGridGroupedCell();
		fgroup2.setLabel("Person Group");
		fgroup2.getGroupables().add(fgroup1);
		fgroup2.getGroupables().add(colAge);

		footerRowPersonInfo = fact.createCxGridFooterRow();
		footerRowPersonInfo.getGroupings().add(fgroup2);
		cxGrid.getFooters().add(footerRowPersonInfo);

		customCellBirthdayFooter = fact.createCxGridMetaCell();
		customCellBirthdayFooter.setElement(ExtensionModelFactory.eINSTANCE
				.createYNumericField());
		customCellBirthdayFooter.setTarget(colBirthday);
		footerRowPersonInfo.getCustomCells().add(customCellBirthdayFooter);

		return cxGrid;
	}

	/**
	 * A simple I18n service implementation.
	 */
	private static class I18nService extends AbstractDisposable implements
			II18nService {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.osbp.runtime.common.i18n.II18nService#getValue(java.lang
		 * .String, java.util.Locale)
		 */
		@Override
		public String getValue(String i18nKey, Locale locale) {
			if (i18nKey.equals("save.i18n")) {
				return "do save";
			} else if (i18nKey.equals("cancel.i18n")) {
				return "do cancel";
			} else if (i18nKey
					.equals("org.eclipse.osbp.ecview.core.common.i18n.TextSearchFieldTooltip")) {
				return "Searchfield accepting wildcards like *, >, <,...";
			} else if (i18nKey
					.equals("org.eclipse.osbp.ecview.core.common.i18n.NumberSearchFieldTooltip")) {
				return "Searchfield accepting wildcards like >, <,...";
			} else if (i18nKey
					.equals("org.eclipse.osbp.ecview.core.common.i18n.BooleanSearchFieldTooltip")) {
				return "Search for Boolean values";
			}
			return i18nKey;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.osbp.runtime.common.dispose.AbstractDisposable#
		 * internalDispose()
		 */
		@Override
		protected void internalDispose() {
			// nothing to do
		}
	}

	/**
	 * A singleton sample memento manager which provides the same memento to all
	 * instances of ECViewGridSampleUI.
	 */
	public class SampleMementoManager implements IECViewMementoManager {

		/** The Constant USER_ID. */
		private static final String USER_ID = "Admin1";

		/**
		 * Instantiates a new sample memento manager.
		 */
		private SampleMementoManager() {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.osbp.ecview.core.common.memento.IECViewMementoManager
		 * #loadMemento(java.lang.String)
		 */
		@Override
		public Object loadMemento(String mementoId) {
			return delegatingMementoManager.loadMemento(USER_ID, mementoId);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.osbp.ecview.core.common.memento.IECViewMementoManager
		 * #saveMemento(java.lang.String, java.lang.Object)
		 */
		@Override
		public IStatus saveMemento(String mementoId, Object memento) {
			return delegatingMementoManager.saveMemento(USER_ID, mementoId,
					memento);
		}

	}
}
