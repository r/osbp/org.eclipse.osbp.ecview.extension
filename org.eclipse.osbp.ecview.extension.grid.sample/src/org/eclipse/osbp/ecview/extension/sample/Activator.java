/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.sample;

import javax.servlet.ServletException;

import org.eclipse.osbp.runtime.common.event.IEventBroker;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * Activator is used by OSGi framework to notify about the start and stop of the
 * bundle. The activator will look for the HttpService and registers the vaadin
 * servlet at it.
 */
public class Activator implements BundleActivator,
		ServiceTrackerCustomizer<HttpService, HttpService>, BundleListener {

	/** The context. */
	private static BundleContext context;
	
	/** The instance. */
	private static Activator INSTANCE;

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	static BundleContext getContext() {
		return context;
	}

	/**
	 * Gets the single instance of Activator.
	 *
	 * @return the instance
	 */
	public static Activator getInstance() {
		return INSTANCE;
	}

	/** The tracker. */
	// used to track the HttpService
	private ServiceTracker<HttpService, HttpService> tracker;
	
	/** The http service. */
	// used to register servlets
	private HttpService httpService;
	
	/** The resource provider. */
	private ResourceProvider resourceProvider;
	
	/** The event broker. */
	private IEventBroker eventBroker;

	/**
	 * Gets the event broker.
	 *
	 * @return the eventBroker
	 */
	public IEventBroker getEventBroker() {
		return eventBroker;
	}

	//
	// Helper methods to get an instance of the http service
	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
	 */
	//
	@Override
	public HttpService addingService(ServiceReference<HttpService> reference) {
		httpService = context.getService(reference);

		try {
			// register the servlet at the http service
			httpService.registerServlet("/", new SimpleVaadinServlet(), null,
					resourceProvider);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (NamespaceException e) {
			e.printStackTrace();
		}

		return httpService;
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void removedService(ServiceReference<HttpService> reference,
			HttpService service) {
		// unregister the servlet from the http service
		httpService.unregister("/");
	}

	/* (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void modifiedService(ServiceReference<HttpService> reference,
			HttpService service) {

	}

	/* (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		INSTANCE = this;
		startBundle(bundleContext, "org.eclipse.osbp.runtime.event");

		ServiceReference<IEventBroker> eventBrokerRef = bundleContext
				.getServiceReference(IEventBroker.class);
		if (eventBrokerRef != null) {
			eventBroker = bundleContext.getService(eventBrokerRef);
		} else {
			throw new RuntimeException("Can not run Bundle properly!");
		}

		resourceProvider = new ResourceProvider();

		handleStartedBundles(context);

		// register this instance as a bundle listener to an reference to all
		// vaadin bundles. Used to find the static resources.
		bundleContext.addBundleListener(this);

		// Start a HttpService-Tracker to get an instance of HttpService
		tracker = new ServiceTracker<>(bundleContext, HttpService.class, this);
		tracker.open();
	}

	/**
	 * Start bundle.
	 *
	 * @param bundleContext
	 *            the bundle context
	 * @param bundleSymbolicName
	 *            the bundle symbolic name
	 * @throws BundleException
	 *             the bundle exception
	 */
	protected void startBundle(BundleContext bundleContext,
			String bundleSymbolicName) throws BundleException {
		for (Bundle bundle : bundleContext.getBundles()) {
			if (bundle.getSymbolicName().equals(bundleSymbolicName)) {
				if (bundle.getState() != Bundle.ACTIVE) {
					bundle.start();
					return;
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		// close the HttpService-tracker
		tracker.close();
		tracker = null;

		resourceProvider = null;

		bundleContext.removeBundleListener(this);

		INSTANCE = null;
		Activator.context = null;
	}

	/**
	 * Tries to find proper started bundles and adds them to resource provider.
	 * Since bundle changed listener will not find them.
	 *
	 * @param context
	 *            the context
	 */
	protected void handleStartedBundles(BundleContext context) {
		for (Bundle bundle : context.getBundles()) {
			String name = bundle.getSymbolicName();
			if (name.startsWith("com.vaadin")
					|| name.equals("org.eclipse.osbp.ecview.extension.grid.sample")
					|| name.equals("org.eclipse.osbp.ecview.extension.widgetset")) {
				resourceProvider.add(bundle);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.osgi.framework.BundleListener#bundleChanged(org.osgi.framework.BundleEvent)
	 */
	@Override
	public void bundleChanged(BundleEvent event) {
		// tracks the starting and stopping of vaadin bundles. If a bundle is a
		// vaadin bundle it will be added to the resource provider for lookups.
		String name = event.getBundle().getSymbolicName();
		if (name.startsWith("com.vaadin")
				|| name.equals("org.eclipse.osbp.ecview.extension.grid.sample")
				|| name.equals("org.eclipse.osbp.ecview.extension.widgetset")) {
			if (event.getType() == BundleEvent.STARTED) {
				resourceProvider.add(event.getBundle());
			} else if (event.getType() == BundleEvent.STOPPED) {
				resourceProvider.remove(event.getBundle());
			}
		}
	}

}
