/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.sample.item.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;

import javax.validation.Valid;

import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;

@SuppressWarnings("all")
public class TelevisionDto extends ItemDto implements IDto, Serializable,
		PropertyChangeListener {
	@Valid
	private ResolutionDto resolution;

	@DomainReference
	@Valid
	private TelevisionDetailsDto details;

	@DomainReference
	@Valid
	private TelevisionDetails2Dto details2;

	/**
	 * Checks whether the object is disposed.
	 * 
	 * @throws RuntimeException
	 *             if the object is disposed.
	 */
	private void checkDisposed() {
		if (isDisposed()) {
			throw new RuntimeException("Object already disposed: " + this);
		}
	}

	/**
	 * Calling dispose will destroy that instance. The internal state will be
	 * set to 'disposed' and methods of that object must not be used anymore.
	 * Each call will result in runtime exceptions.<br/>
	 * If this object keeps composition containments, these will be disposed
	 * too. So the whole composition containment tree will be disposed on
	 * calling this method.
	 */
	@Dispose
	public void dispose() {
		if (isDisposed()) {
			return;
		}
		try {
			if (this.details != null) {
				this.details.dispose();
				this.details = null;
			}
			if (this.details2 != null) {
				this.details2.dispose();
				this.details2 = null;
			}

		} finally {
			super.dispose();
		}

	}

	/**
	 * Returns the resolution property.
	 */
	public ResolutionDto getResolution() {
		if (this.resolution == null) {
			this.resolution = new ResolutionDto();
		}
		return this.resolution;
	}

	/**
	 * Sets the <code>resolution</code> property to this instance.
	 * 
	 * @param resolution
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setResolution(final ResolutionDto resolution) {
		if (this.resolution != null) {
			this.resolution.removePropertyChangeListener(this);
		}

		firePropertyChange("resolution", this.resolution,
				this.resolution = resolution);

		if (this.resolution != null) {
			this.resolution.addPropertyChangeListener(this);
		}
	}

	/**
	 * Returns the details property or <code>null</code> if not present.
	 */
	public TelevisionDetailsDto getDetails() {
		return this.details;
	}

	/**
	 * Sets the <code>details</code> property to this instance.
	 * 
	 * @param details
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setDetails(final TelevisionDetailsDto details) {
		checkDisposed();
		firePropertyChange("details", this.details, this.details = details);
	}

	/**
	 * Returns the details2 property or <code>null</code> if not present.
	 */
	public TelevisionDetails2Dto getDetails2() {
		return this.details2;
	}

	/**
	 * Sets the <code>details2</code> property to this instance.
	 * 
	 * @param details2
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setDetails2(final TelevisionDetails2Dto details2) {
		checkDisposed();
		firePropertyChange("details2", this.details2, this.details2 = details2);
	}

	public TelevisionDto createDto() {
		return new TelevisionDto();
	}

}
