/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.sample.item.dtos;

import org.eclipse.osbp.sample.item.dtos.BaseUUIDDto;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Date;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@SuppressWarnings("all")
public class DetailsDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  @Valid
  private Date guaranteeEnd;
  
/**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
/**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
/**
   * Returns the guaranteeEnd property or <code>null</code> if not present.
   */
  public Date getGuaranteeEnd() {
    return this.guaranteeEnd;
  }
  
/**
   * Sets the <code>guaranteeEnd</code> property to this instance.
   * 
   * @param guaranteeEnd - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setGuaranteeEnd(final Date guaranteeEnd) {
    firePropertyChange("guaranteeEnd", this.guaranteeEnd, this.guaranteeEnd = guaranteeEnd );
  }
  
  public DetailsDto createDto() {
    return new DetailsDto();
  }
  
}
