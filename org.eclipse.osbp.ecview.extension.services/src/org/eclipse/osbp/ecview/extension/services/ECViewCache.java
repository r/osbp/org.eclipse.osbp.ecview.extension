/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.services;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.osbp.ecview.core.common.extender.IECViewCache;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;

/**
 * The Class ECViewCache.
 */
@Component
public class ECViewCache implements IECViewCache {

	/** The Constant ECVIEW_XMI. */
	public static final String ECVIEW_XMI = "ecview";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ECViewCache.class);

	/** The resource set. */
	private ResourceSetImpl resourceSet;

	/**
	 * Activate.
	 *
	 * @param context
	 *            the context
	 */
	@Activate
	protected void activate(ComponentContext context) {
		resourceSet = new ResourceSetImpl();

		// do EMF registration
		EPackage.Registry.INSTANCE.put(CoreModelPackage.eINSTANCE.getNsURI(),
				CoreModelPackage.eINSTANCE);
		EPackage.Registry.INSTANCE.put(
				ExtensionModelPackage.eINSTANCE.getNsURI(),
				ExtensionModelPackage.eINSTANCE);
		EPackage.Registry.INSTANCE.put(YECviewPackage.eINSTANCE.getNsURI(),
				YECviewPackage.eINSTANCE);
		if (!Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap()
				.containsKey(ECVIEW_XMI)) {
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
					ECVIEW_XMI, new XMIResourceFactoryImpl());
		}
	}

	/**
	 * Deactivate.
	 *
	 * @param context
	 *            the context
	 */
	@Deactivate
	protected void deactivate(ComponentContext context) {
		resourceSet.getResources().clear();
		resourceSet = null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.extender.IECViewCache#getView(java.lang.String)
	 */
	@Override
	public YView getView(String id) {
		if (id == null) {
			return null;
		}
		YView cxSource = resourceSet.getResources().stream()
				.filter(r -> r.getContents().size() > 0)
				.map(r -> (YView) r.getContents().get(0)).filter(g -> {
//					return id.equals(g.getId());
					return id.equals(g.getViewName());
				}).findFirst().orElse(null);

		return cxSource != null ? EcoreUtil.copy(cxSource) : null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.extender.IECViewCache#getViews(java.lang.Class, java.lang.String)
	 */
	@Override
	public List<YView> getViews(Class<?> inputType, String beanSlot) {
		if (inputType == null) {
			return Collections.emptyList();
		}

		List<YView> result = new ArrayList<>();
		for (Resource resource : new ArrayList<>(resourceSet.getResources())) {
			if (resource.getContents().isEmpty()) {
				continue;
			}
			YView source = (YView) resource.getContents().get(0);
			if (source == null) {
				continue;
			}

			if (isFor(source, inputType, beanSlot)) {
				result.add(EcoreUtil.copy(source));
			}
		}

		return result;
	}

	/**
	 * Checks if is for.
	 *
	 * @param source
	 *            the source
	 * @param inputType
	 *            the input type
	 * @param beanSlot
	 *            the bean slot
	 * @return true, if is for
	 */
	private boolean isFor(YView source, Class<?> inputType, String beanSlot) {
		for (YBeanSlot slot : source.getBeanSlots()) {
			if(beanSlot != null && !beanSlot.equals(slot.getName())) {
				// skip the beanSlot with a different name
				continue;
			}
			
			if (slot.getValueType() == inputType
					&& inputType.getCanonicalName().equals(
							slot.getValueTypeQualifiedName())) {
				return true;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.extender.IECViewCache#registerViews(java.util.List)
	 */
	@Override
	public void registerViews(List<URL> urls) {
		for (URL url : urls) {
			Resource resource = resourceSet.getResource(
					URI.createURI(url.toString()), true);
			try {
				resource.load(null);
			} catch (IOException e) {
				LOGGER.error("{}", e);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.extender.IECViewCache#unregisterViews(java.util.List)
	 */
	@Override
	public void unregisterViews(List<URL> urls) {
		for (URL url : urls) {
			Resource resource = resourceSet.getResource(
					URI.createURI(url.toString()), false);
			if (resource != null) {
				resource.unload();
				resourceSet.getResources().remove(resource);
			}
		}
	}
}
