/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessorEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.editparts.IInternalSuspectLayoutingInfoEditpart;
import org.eclipse.osbp.ecview.extension.editparts.ILayoutingInfoEditpart;
import org.eclipse.osbp.ecview.extension.editparts.ISuspectLayoutingInfoEditpart;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link ISuspectLayoutingInfoEditpart}.
 */
public class SuspectLayoutingInfoEditpart extends ElementEditpart<YSuspectInfo>
		implements IInternalSuspectLayoutingInfoEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SuspectLayoutingInfoEditpart.class);

	/** The bindings. */
	private List<IBindingEditpart<?>> bindings;

	/** The vps. */
	private List<IVisibilityProcessorEditpart> vps;

	/** The binding set. */
	private IBindingSetEditpart bindingSet;

	/** The active. */
	private boolean active;

	/**
	 * A default constructor.
	 */
	protected SuspectLayoutingInfoEditpart() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * handleModelAdd(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelAdd(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YSUSPECT_INFO__BINDINGS:
			YBinding yBinding = (YBinding) notification.getNewValue();

			IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(
					getContext(), yBinding);
			internalAddBinding(editPart);

			if (active) {
				bindingSet.addTransientBinding(editPart);
			}
			break;
		default:
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * handleModelRemove(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelRemove(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YSUSPECT_INFO__BINDINGS:
			YBinding yBinding = (YBinding) notification.getOldValue();

			IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(
					getContext(), yBinding);
			internalRemoveBinding(editPart);

			if (active) {
				bindingSet.removeTransientBinding(editPart);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Ensures that the internal bindings are loaded properly.
	 */
	private void ensureBindingsLoaded() {
		if (bindings == null) {
			internalLoadBindings();
		}
	}

	/**
	 * Is called to load and initialize all bindings.
	 */
	protected void internalLoadBindings() {
		checkDisposed();

		if (bindings == null) {
			bindings = new ArrayList<IBindingEditpart<?>>();
			for (YBinding yBindingEP : getModel().getBindings()) {
				IBindingEditpart<?> editPart = getEditpart(getContext(),
						yBindingEP);
				internalAddBinding(editPart);
			}
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of bindings.
	 * 
	 * @param editpart
	 *            The editpart to be added
	 */
	protected void internalAddBinding(IBindingEditpart<?> editpart) {
		checkDisposed();

		ensureBindingsLoaded();
		if (!bindings.contains(editpart)) {
			bindings.add(editpart);
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of bindings.
	 * 
	 * @param editpart
	 *            The editpart to be removed
	 */
	protected void internalRemoveBinding(IBindingEditpart<?> editpart) {
		checkDisposed();

		if (bindings != null && editpart != null) {
			bindings.remove(editpart);
		}
	}

	/**
	 * Gets the bindings.
	 *
	 * @return the bindings
	 */
	public List<IBindingEditpart<?>> getBindings() {
		ensureBindingsLoaded();
		return Collections.unmodifiableList(bindings);
	}

	/**
	 * Ensures that the internal vps are loaded properly.
	 */
	private void ensureVPsLoaded() {
		if (vps == null) {
			internalLoadVPs();
		}
	}

	/**
	 * Is called to load and initialize all VPs.
	 */
	protected void internalLoadVPs() {
		checkDisposed();

		if (vps == null) {
			vps = new ArrayList<IVisibilityProcessorEditpart>();
			for (YVisibilityProcessor yModel : getModel()
					.getVisibilityProcessors()) {
				IVisibilityProcessorEditpart editPart = getEditpart(
						getContext(), yModel);
				internalAddVP(editPart);
			}
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of vps.
	 * 
	 * @param editpart
	 *            The editpart to be added
	 */
	protected void internalAddVP(IVisibilityProcessorEditpart editpart) {
		checkDisposed();

		ensureVPsLoaded();
		if (!vps.contains(editpart)) {
			vps.add(editpart);
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of vps.
	 * 
	 * @param editpart
	 *            The editpart to be removed
	 */
	protected void internalRemoveVP(IVisibilityProcessorEditpart editpart) {
		checkDisposed();

		if (vps != null && editpart != null) {
			vps.remove(editpart);
		}
	}

	/**
	 * Gets the vps.
	 *
	 * @return the vps
	 */
	public List<IVisibilityProcessorEditpart> getVPs() {
		ensureVPsLoaded();
		return Collections.unmodifiableList(vps);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * internalDispose()
	 */
	@Override
	protected void internalDispose() {

		try {
			LOGGER.info("Disposing " + toString());

			active = false;

			for (IVisibilityProcessorEditpart vp : getVPs()) {
				vp.dispose();
			}
			vps = null;

			for (IBindingEditpart<?> binding : getBindings()) {
				// remove the binding -> Will be automatically disposed
				bindingSet.removeTransientBinding(binding);
			}
			bindings = null;
			bindingSet = null;

		} finally {
			super.internalDispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.extension.editparts.ISuspectLayoutingInfoEditpart
	 * #getView()
	 */
	public IViewEditpart getView() {
		YView yView = ((YStrategyLayout) getModel().eContainer().eContainer())
				.getView();
		;
		return yView != null ? (IViewEditpart) getEditpart(getContext(), yView)
				: null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * requestDispose()
	 */
	public void requestDispose() {
		throw new UnsupportedOperationException("Implement later or remove");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.extension.editparts.ISuspectLayoutingInfoEditpart
	 * #getParent()
	 */
	@Override
	public ILayoutingInfoEditpart getParent() {
		YLayoutingInfo cxLayout = (YLayoutingInfo) getModel().eContainer();
		return cxLayout != null ? (ILayoutingInfoEditpart) getEditpart(
				getContext(), cxLayout) : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.extension.editparts.
	 * IInternalSuspectLayoutingInfoEditpart#activate()
	 */
	@Override
	public void activate() {
		if (active) {
			return;
		}

		try {
			// hold a reference to the bindingSet
			bindingSet = getView().getBindingSet();
			
			if(getBindings().size() == 2){
				// TODO for debug
				LOGGER.debug("Size bindingset = 2");
			}
			for (IBindingEditpart<?> binding : getBindings()) {
				// else add it
				bindingSet.addTransientBinding(binding);
			}
		} finally {
			active = true;
		}
	}

	@Override
	public void postActivate() {
		for (IVisibilityProcessorEditpart vp : getVPs()) {
			vp.activate();
		}
	}
}
