/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.strategies;

import java.util.Collection;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.extension.api.ILayoutingStrategyProvider;
import org.eclipse.osbp.ecview.extension.editparts.strategies.IDefaultLayoutingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.editparts.strategies.IFocusingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.model.YDefaultLayoutingStrategy;
import org.eclipse.osbp.runtime.common.event.IEventBroker;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * See {@link IFocusingStrategyEditpart}.
 */
public class DefaultLayoutingStrategyEditpart extends
		LayoutingStrategyEditpart<YDefaultLayoutingStrategy> implements
		IDefaultLayoutingStrategyEditpart, EventHandler {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DefaultLayoutingStrategyEditpart.class);

	/**
	 * The default constructor.
	 */
	protected DefaultLayoutingStrategyEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.emf.strategies.LayoutingStrategyEditpart#initialize(org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy)
	 */
	@Override
	public void initialize(IViewContext context, YDefaultLayoutingStrategy model) {
		super.initialize(context, model);

		setDefaultStrategy();

		IViewContext viewContext = getView().getContext();
		IEventBroker eventBroker = viewContext.getService(IEventBroker.class
				.getName());
		if (eventBroker == null) {
			LOGGER.error("Eventbroker not available!");
			return;
		}

		eventBroker.subscribe(ILayoutingStrategyProvider.ACTIVE_PROVIDER_TOPIC, this);
	}

	/**
	 * Sets the default strategy.
	 */
	protected void setDefaultStrategy() {
		ILayoutingStrategyProvider provider = findDefaultService();
		if (provider == null) {
				LOGGER.error("IDefaultLayoutingStrategyProvider not available!");
				return;
		}
		this.strategy = provider.getStrategy();
	}
	
	
	/**
	 * Finds the default service.
	 *
	 * @return the i layouting strategy provider
	 */
	private ILayoutingStrategyProvider findDefaultService() {
		BundleContext context = FrameworkUtil.getBundle(getClass())
				.getBundleContext();

		try {
			Collection<ServiceReference<ILayoutingStrategyProvider>> references = context
					.getServiceReferences(ILayoutingStrategyProvider.class,
							"(ecview.layouting.id=Default)");
			if (!references.isEmpty()) {
				ILayoutingStrategyProvider provider = context
						.getService(references.iterator().next());
				return provider;
			}
		} catch (InvalidSyntaxException e) {
			LOGGER.warn("{}", e);
		}
		return null;
	}


	/* (non-Javadoc)
	 * @see org.osgi.service.event.EventHandler#handleEvent(org.osgi.service.event.Event)
	 */
	@Override
	public void handleEvent(Event event) {
		checkDisposed();

		ILayoutingStrategyProvider result = (ILayoutingStrategyProvider) event
				.getProperty(IEventBroker.DATA);
		if (result != null) {
			this.strategy = result.getStrategy();

			// call the parent to do layouting
			getParent().requestLayouting();
		} else {
			LOGGER.warn("Strategy was null. So current strategy was not replaced.");
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.emf.strategies.LayoutingStrategyEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			IEventBroker eventBroker = getView().getContext().getService(
					IEventBroker.class.getName());
			if (eventBroker == null) {
				LOGGER.error("Eventbroker not available! Could not unsubscribe.");
			} else {
				eventBroker.unsubscribe(this);
			}
		} finally {
			super.internalDispose();
		}
	}

}
