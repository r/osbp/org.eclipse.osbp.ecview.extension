/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf;

import org.eclipse.osbp.ecview.core.common.editpart.emf.FieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IRichTextAreaEditpart;
import org.eclipse.osbp.ecview.extension.editparts.ISuspectEditpart;
import org.eclipse.osbp.ecview.extension.model.YRichTextArea;

/**
 * Implementation of {@link ISuspectEditpart}.
 */
public class RichTextAreaEditpart extends
		FieldEditpart<YRichTextArea> implements IRichTextAreaEditpart {

	/**
	 * A default constructor.
	 */
	protected RichTextAreaEditpart() {
	}

}
