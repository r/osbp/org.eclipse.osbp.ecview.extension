/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.strategies;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.extension.api.IFocusingStrategyProvider;
import org.eclipse.osbp.ecview.extension.editparts.strategies.IDelegatingFocusingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * See {@link IDelegatingFocusingStrategyEditpart}.
 */
public class DelegatingFocusingStrategyEditpart extends
		FocusingStrategyEditpart<YDelegatingFocusingStrategy> implements
		IDelegatingFocusingStrategyEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DefaultLayoutingStrategyEditpart.class);

	/**
	 * The default constructor.
	 */
	protected DelegatingFocusingStrategyEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#initialize(org.eclipse.osbp.ecview.core.common.model.core.YElement)
	 */
	@Override
	public void initialize(IViewContext context, YDelegatingFocusingStrategy model) {
		applyInitialStrategy(model);

		super.initialize(context, model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YDELEGATING_FOCUSING_STRATEGY__DELEGATE_STRATEGY_ID:
			internalSetDelegate(notification.getNewStringValue());
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/**
	 * Applies the initial strategy and ensures that key strokes are available.
	 *
	 * @param model
	 *            the model
	 */
	protected void applyInitialStrategy(YDelegatingFocusingStrategy model) {
		IFocusingStrategyProvider result = findService(model
				.getDelegateStrategyId());
		if (result != null) {
			this.strategy = result.getStrategy();
		} else {
			LOGGER.warn("Strategy was null. So current strategy was not replaced.");
		}

		internalSetKeystroke(model);
	}

	/**
	 * Internal set delegate.
	 *
	 * @param id
	 *            the id
	 */
	public void internalSetDelegate(String id) {
		checkDisposed();

		IFocusingStrategyProvider result = findService(id);
		if (result != null) {
			this.strategy = result.getStrategy();
			// key key stroke definitions
			internalSetKeystroke();
		} else {
			LOGGER.warn("Strategy was null. So current strategy was not replaced.");
		}
	}

	/**
	 * Finds the service with the given id.
	 *
	 * @param id
	 *            the id
	 * @return the i focusing strategy provider
	 */
	private IFocusingStrategyProvider findService(String id) {
		BundleContext context = FrameworkUtil.getBundle(getClass())
				.getBundleContext();

		try {
			Collection<ServiceReference<IFocusingStrategyProvider>> references = context
					.getServiceReferences(IFocusingStrategyProvider.class,
							String.format("(ecview.focusing.id=%s)", id));
			if (!references.isEmpty()) {
				IFocusingStrategyProvider provider = context
						.getService(references.iterator().next());
				return provider;
			}
		} catch (InvalidSyntaxException e) {
			LOGGER.warn("{}", e);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.emf.strategies.FocusingStrategyEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			LOGGER.info("Disposing " + toString());

			viewContext = null;
		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.emf.strategies.FocusingStrategyEditpart#doFocus(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void doFocus(Object source, Object target) {
		checkDisposed();

		if (strategy != null) {
			strategy.focus(source, target, (YStrategyLayout) getParent()
					.getModel());
		}
	}

}
