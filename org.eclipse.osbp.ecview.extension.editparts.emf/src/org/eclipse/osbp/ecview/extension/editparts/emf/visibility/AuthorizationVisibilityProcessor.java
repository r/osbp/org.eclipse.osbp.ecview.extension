/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.visibility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleEvent;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleHandler;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleService;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization.Action;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization.Group;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;

/**
 * The Class AuthorizationVisibilityProcessor.
 */
public class AuthorizationVisibilityProcessor implements IVisibilityProcessor, ILifecycleHandler {

	/** The handlers. */
	private Set<Handler> handlers = new HashSet<>();

	/** The input. */
	@SuppressWarnings("unused")
	private Object input;

	/** The context. */
	private IViewContext context;

	/** The lifecycle service. */
	private ILifecycleService lifecycleService;

	/** The y view. */
	private YView yView;

	/** The manager. */
	private IVisibilityManager manager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor#init
	 * (org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager)
	 */
	@Override
	public void init(IVisibilityManager manager) {
		this.manager = manager;
		List<YEmbeddable> yEmbeddables = collectAllEmbeddables();
		for (YEmbeddable target : yEmbeddables) {
			// do not create handlers for layouts, tabSheets,...
			if (target instanceof YField && target.getId() != null) {
				handlers.add(new Handler(manager, target));
			}
		}
		lifecycleService = context.getService(ILifecycleService.class.getName());
		lifecycleService.addHandler(this);

		/*
		 * This VP is not triggered by changing data. And at this point, the vp
		 * becomes activated the first time -> we need to fire initially.
		 * 
		 * PLEASE DO NOT REMOVE!
		 */
		fire();
	}

	/**
	 * Setup.
	 *
	 * @param context
	 *            the context
	 * @param yVP
	 *            the y vp
	 */
	public void setup(IViewContext context, YAuthorizationVisibilityProcessor yVP) {

		yView = (YView) context.getViewEditpart().getModel();

		this.context = context;
	}

	/**
	 * Find all embeddables in the given yView.<br>
	 * The ui is rendered now.
	 *
	 * @return the list
	 */
	private List<YEmbeddable> collectAllEmbeddables() {
		List<YEmbeddable> yEmbeddables = new ArrayList<>();
		TreeIterator<EObject> iter = EcoreUtil.getAllProperContents(yView, true);
		while (iter.hasNext()) {
			EObject eObject = iter.next();
			if (eObject instanceof YEmbeddable) {
				yEmbeddables.add((YEmbeddable) eObject);
			}
		}
		return yEmbeddables;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.notification.ILifecycleHandler#
	 * notifyLifecycle
	 * (org.eclipse.osbp.ecview.core.common.notification.ILifecycleEvent)
	 */
	@Override
	public void notifyLifecycle(ILifecycleEvent event) {
		Object model = event.getEditpart().getModel();
		if (model instanceof YEmbeddable) {
			Handler handler = findHandler((YEmbeddable) model);
			if (ILifecycleEvent.TYPE_UNRENDERED.equals(event.getType())) {
				if (handler != null) {
					handlers.remove(handler);
				}
			} else if (ILifecycleEvent.TYPE_DISPOSED.equals(event.getType())) {
				if (handler != null) {
					handlers.remove(handler);
				}
				if (model == yView) {
					// means the view was disposed!
					internalDispose();
				}
			} else if (ILifecycleEvent.TYPE_RENDERED.equals(event.getType())) {
				if (handler == null) {
					handlers.add(new Handler(manager, (YEmbeddable) model));
				}
			}
		}
	}

	/**
	 * Internal dispose.
	 */
	private void internalDispose() {
		lifecycleService.removeHandler(this);
		lifecycleService = null;

		handlers.clear();
		handlers = null;

		manager = null;
		context = null;
		yView = null;
	}

	/**
	 * Returns the handler for the given model or <code>null</code> if no
	 * handler is available.
	 *
	 * @param model
	 *            the model
	 * @return the handler
	 */
	private Handler findHandler(YEmbeddable model) {
		for (Handler handler : handlers) {
			if (handler.model == model) {
				return handler;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor#fire
	 * ()
	 */
	@Override
	public void fire() {
		doFire();

		for (Handler handler : handlers) {
			handler.apply();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor#
	 * setInput (java.lang.Object)
	 */
	@Override
	public void setInput(Object input) {
		this.input = input;

		fire();
	}

	/**
	 * Do fire.
	 */
	public void doFire() {
		IUserAccessService userAccessService = context.getService(IUserAccessService.class.getName());
		for (Handler handler : handlers) {
			handler.handle(userAccessService);
		}
	}

	/**
	 * The Class Handler.
	 */
	private static class Handler {

		/** The target. */
		private IVisibilityHandler target;

		/** The field id. */
		private String fieldId;

		/** The entity name. */
		private String dtoName;

		/** The entity property. */
		private String dtoProperty;

		/** The model. */
		private YEmbeddable model;

		/**
		 * Instantiates a new handler.
		 *
		 * @param manager
		 *            the manager
		 * @param model
		 *            the model
		 */
		public Handler(IVisibilityManager manager, YEmbeddable model) {
			this.model = model;
			fieldId = model.getId();
			dtoName = model.getAuthorizationGroup();
			dtoProperty = model.getAuthorizationId();

			target = manager.getById(fieldId);
		}

		/**
		 * Handle.
		 *
		 * @param userAccessService
		 *            the user access service
		 */
		public void handle(IUserAccessService userAccessService) {

			if (target == null) {
				return;
			}

			target.setVisible(false);
			target.setEditable(!model.isReadonly());
			target.setEnabled(false);
			if (userAccessService == null) {
				target.setVisible(true);
			} else {
				boolean dtoGrant = userAccessService.isGranted(Group.DTO, Action.READABLE, dtoName);
				if (dtoGrant) {
					target.setVisible(!userAccessService.isVetoed(Group.DTO, Action.INVISIBLE, dtoName, dtoProperty));
					if (!model.isReadonly()) {
						target.setEditable(
								!userAccessService.isVetoed(Group.DTO, Action.NONEDITABLE, dtoName, dtoProperty));
					}
					target.setEnabled(!userAccessService.isVetoed(Group.DTO, Action.DISABLED, dtoName, dtoProperty));
				} else {
					boolean beanGrant = userAccessService.isGranted(Group.BEAN, Action.READABLE, dtoName);
					if (beanGrant) {
						target.setVisible(
								!userAccessService.isVetoed(Group.BEAN, Action.INVISIBLE, dtoName, dtoProperty));
						if (!model.isReadonly()) {
							target.setEditable(
									!userAccessService.isVetoed(Group.BEAN, Action.NONEDITABLE, dtoName, dtoProperty));
						}
						target.setEnabled(
								!userAccessService.isVetoed(Group.BEAN, Action.DISABLED, dtoName, dtoProperty));
					}
				}
			}
		}

		/**
		 * Apply.
		 */
		public void apply() {
			if (target != null) {
				target.apply();
			}
		}
	}
}
