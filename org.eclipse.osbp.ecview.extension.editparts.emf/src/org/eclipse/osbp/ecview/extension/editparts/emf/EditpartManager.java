/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.common.AbstractEditpartManager;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.extension.editparts.emf.components.BlobUploadComponentEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.components.ContentSensitiveLayoutEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.components.CustomDecimalFieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.components.I18nComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.components.IconComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.components.PairComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.CustomDecimalConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.DecimalToUomoConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.NumericToResourceConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.NumericToUomoConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.ObjectToStringConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.PriceToStringConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.QuantityToStringConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.SimpleDecimalConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.StringToByteArrayConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.StringToResourceConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.converter.VaaclipseUiThemeToStringConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.strategies.DefaultLayoutingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.strategies.DelegatingFocusingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.strategies.DelegatingLayoutingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.visibility.AuthorizationVisibilityProcessorEditpart;
import org.eclipse.osbp.ecview.extension.editparts.emf.visibility.SubTypeVisibilityProcessorEditpart;
import org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent;
import org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout;
import org.eclipse.osbp.ecview.extension.model.YCustomDecimalField;
import org.eclipse.osbp.ecview.extension.model.YDefaultLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YI18nComboBox;
import org.eclipse.osbp.ecview.extension.model.YIconComboBox;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField;
import org.eclipse.osbp.ecview.extension.model.YMaskedNumericField;
import org.eclipse.osbp.ecview.extension.model.YMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YPairComboBox;
import org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YQuantityTextField;
import org.eclipse.osbp.ecview.extension.model.YRichTextArea;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YObjectToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToByteArrayConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YVaaclipseUiThemeToStringConverter;
import org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityPackage;
import org.osgi.service.component.ComponentContext;

/**
 * An implementation of IEditPartManager for eObjects with
 * nsURI=http://eclipse.org/emf/emfclient/uimodel.
 */
public class EditpartManager extends AbstractEditpartManager {

	/**
	 * Activate.
	 *
	 * @param context
	 *            the context
	 */
	protected void activate(ComponentContext context) {

	}

	/**
	 * Deactivate.
	 *
	 * @param context
	 *            the context
	 */
	protected void deactivate(ComponentContext context) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IEditPartManager#isFor(java
	 * .lang.Object)
	 */
	@Override
	public boolean isFor(Object element) {
		if (element instanceof EObject) {
			String uriString = ((EObject) element).eClass().getEPackage().getNsURI();
			return uriString.equals(YECviewPackage.eNS_URI) || uriString.equals(YConverterPackage.eNS_URI)
					|| uriString.equals(YVisibilityPackage.eNS_URI);
		} else if (element instanceof String) {
			return element.equals(YECviewPackage.eNS_URI) || element.equals(YConverterPackage.eNS_URI)
					|| element.equals(YVisibilityPackage.eNS_URI);
		}
		return false;
	}

	/**
	 * Creates a new instance of the edit part.
	 * 
	 * @param <A>
	 *            an instance of {@link IElementEditpart}
	 * @param yElement
	 *            the model element
	 * @return editpart
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IElementEditpart> A createEditpart(IViewContext context, Object yElement) {
		// asserts that no editpart was created already for the given element
		assertOneEditpartForModelelement(yElement);

		ElementEditpart<YElement> result = null;
		if (yElement instanceof YStrategyLayout) {
			result = createNewInstance(StrategyLayoutEditpart.class);
		} else if (yElement instanceof YDefaultLayoutingStrategy) {
			result = createNewInstance(DefaultLayoutingStrategyEditpart.class);
		} else if (yElement instanceof YDelegatingLayoutingStrategy) {
			result = createNewInstance(DelegatingLayoutingStrategyEditpart.class);
		} else if (yElement instanceof YSuspect) {
			result = createNewInstance(SuspectEditpart.class);
		} else if (yElement instanceof YSuspectInfo) {
			result = createNewInstance(SuspectLayoutingInfoEditpart.class);
		} else if (yElement instanceof YLayoutingInfo) {
			result = createNewInstance(LayoutingInfoEditpart.class);
		} else if (yElement instanceof YDelegatingFocusingStrategy) {
			result = createNewInstance(DelegatingFocusingStrategyEditpart.class);
			// custom component BlobUploadComponent
		} else if (yElement instanceof YBlobUploadComponent) {
			result = createNewInstance(BlobUploadComponentEditpart.class);
		} else if (yElement instanceof YCustomDecimalField) {
			result = createNewInstance(CustomDecimalFieldEditpart.class);
		} else if (yElement instanceof YObjectToStringConverter) {
			result = createNewInstance(ObjectToStringConverterEditpart.class);
		} else if (yElement instanceof YVaaclipseUiThemeToStringConverter) {
			result = createNewInstance(VaaclipseUiThemeToStringConverterEditpart.class);
		} else if (yElement instanceof YStringToResourceConverter) {
			result = createNewInstance(StringToResourceConverterEditpart.class);
		} else if (yElement instanceof YNumericToResourceConverter) {
			result = createNewInstance(NumericToResourceConverterEditpart.class);
		} else if (yElement instanceof YQuantityToStringConverter) {
			result = createNewInstance(QuantityToStringConverterEditpart.class);
		} else if (yElement instanceof YPriceToStringConverter) {
			result = createNewInstance(PriceToStringConverterEditpart.class);
		} else if (yElement instanceof YNumericToUomoConverter) {
			result = createNewInstance(NumericToUomoConverterEditpart.class);
		} else if (yElement instanceof YDecimalToUomoConverter) {
			result = createNewInstance(DecimalToUomoConverterEditpart.class);
		} else if (yElement instanceof YCustomDecimalConverter) {
			result = createNewInstance(CustomDecimalConverterEditpart.class);
		} else if (yElement instanceof YSimpleDecimalConverter) {
			result = createNewInstance(SimpleDecimalConverterEditpart.class);
		} else if (yElement instanceof YQuantityTextField) {
			result = createNewInstance(QuantityTextfieldEditpart.class);
		} else if (yElement instanceof YAuthorizationVisibilityProcessor) {
			result = createNewInstance(AuthorizationVisibilityProcessorEditpart.class);
		} else if (yElement instanceof YIconComboBox) {
			result = createNewInstance(IconComboBoxEditpart.class);
		} else if (yElement instanceof YI18nComboBox) {
			result = createNewInstance(I18nComboBoxEditpart.class);
		} else if (yElement instanceof YPairComboBox) {
			result = createNewInstance(PairComboBoxEditpart.class);
		} else if (yElement instanceof YContentSensitiveLayout) {
			result = createNewInstance(ContentSensitiveLayoutEditpart.class);
		} else if (yElement instanceof YSubTypeVisibilityProcessor) {
			result = createNewInstance(SubTypeVisibilityProcessorEditpart.class);
		} else if (yElement instanceof YRichTextArea) {
			result = createNewInstance(RichTextAreaEditpart.class);
		} else if (yElement instanceof YMaskedTextField) {
			result = createNewInstance(MaskedTextFieldEditpart.class);
		} else if (yElement instanceof YMaskedNumericField) {
			result = createNewInstance(MaskedNumericFieldEditpart.class);
		} else if (yElement instanceof YMaskedDecimalField) {
			result = createNewInstance(MaskedDecimalFieldEditpart.class);
		} else if (yElement instanceof YPrefixedMaskedTextField) {
			result = createNewInstance(PrefixedMaskedTextFieldEditpart.class);
		} else if (yElement instanceof YStringToByteArrayConverter) {
			result = createNewInstance(StringToByteArrayConverterEditpart.class);
		}

		if (result != null) {
			result.initialize(context, (YElement) yElement);
		}

		return (A) result;
	}

	/**
	 * Returns a new instance of the type.
	 * 
	 * @param type
	 *            The type of the editpart for which an instance should be
	 *            created.
	 * @return editPart
	 * @throws InstantiationException
	 *             e
	 * @throws IllegalAccessException
	 *             e
	 */
	protected IElementEditpart newInstance(Class<? extends IElementEditpart> type) throws InstantiationException, IllegalAccessException {
		try {
			Constructor<? extends IElementEditpart> c = type.getDeclaredConstructor(new Class<?>[0]);
			if (!c.isAccessible()) {
				c.setAccessible(true);
				return c.newInstance(new Object[0]);
			}
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException(e);
		} catch (SecurityException e) {
			throw new IllegalStateException(e);
		} catch (IllegalArgumentException e) {
			throw new IllegalStateException(e);
		} catch (InvocationTargetException e) {
			throw new IllegalStateException(e);
		}

		return type.newInstance();
	}
}
