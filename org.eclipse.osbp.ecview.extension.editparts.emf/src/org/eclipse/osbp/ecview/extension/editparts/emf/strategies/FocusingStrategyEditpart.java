/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.emf.strategies;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IKeyStrokeDefinitionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YKeyStrokeDefinition;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.impl.custom.KeyStrokeUtil;
import org.eclipse.osbp.ecview.extension.api.IFocusingStrategy;
import org.eclipse.osbp.ecview.extension.editparts.IInternalFocusingEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IStrategyLayoutEditpart;
import org.eclipse.osbp.ecview.extension.editparts.strategies.IFocusingStrategyEditpart;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YFocusingStrategy;
import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * See {@link IFocusingStrategyEditpart}.
 *
 * @param <M>
 *            the generic type
 */
public abstract class FocusingStrategyEditpart<M extends YFocusingStrategy>
		extends ElementEditpart<M> implements IInternalFocusingEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(FocusingStrategyEditpart.class);

	/** The strategy. */
	protected IFocusingStrategy strategy;

	/** The key stroke def. */
	private IKeyStrokeDefinitionEditpart keyStrokeDef;

	/** The view context. */
	protected IViewContext viewContext;

	/**
	 * The default constructor.
	 */
	protected FocusingStrategyEditpart() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStrategyLayoutEditpart getParent() {
		YElement yParent = (YElement) getModel().eContainer();
		return yParent != null ? (IStrategyLayoutEditpart) getEditpart(getContext(),yParent)	: null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.strategies.IFocusingStrategyEditpart#getView()
	 */
	@Override
	public IViewEditpart getView() {
		YView yView = getModel().getView();
		return yView != null ? (IViewEditpart) getEditpart(getContext(),yView) : null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.strategies.IFocusingStrategyEditpart#focus(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void focus(Object source, Object target) {
		doFocus(source, target);
	}

	/**
	 * Do focus.
	 *
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 */
	protected abstract void doFocus(Object source, Object target);

	/**
	 * Internal set keystroke.
	 */
	protected void internalSetKeystroke() {
		internalSetKeystroke(getModel());
	}

	/**
	 * Creates the keystroke based on internal definitions or by the strategy.
	 *
	 * @param model
	 *            the model
	 * @model
	 */
	protected void internalSetKeystroke(M model) {
		if (model.getKeyStrokeDefinition() == null) {
			if (strategy == null) {
				throw new IllegalStateException("Strategy must not be null!");
			}
			KeyStrokeDefinition tempDef = strategy.getKeyStrokeDefinition();
			YKeyStrokeDefinition yDef = KeyStrokeUtil.from(tempDef);
			model.setTempStrokeDefinition(yDef);
		}
	}

	/**
	 * Set the new content.
	 *
	 * @param yDefinition
	 *            the y definition
	 */
	protected void internalSetKeyStrokeDefinition(
			YKeyStrokeDefinition yDefinition) {
		ensureKeyStrokeDefinitionLoaded();

		IKeyStrokeDefinitionEditpart newDefinition = getEditpart(getContext(),yDefinition);
		if (this.keyStrokeDef == newDefinition) {
			return;
		}

		IKeyStrokeDefinitionEditpart oldDefinition = this.keyStrokeDef;
		this.keyStrokeDef = newDefinition;

		if (oldDefinition != null) {
			oldDefinition.dispose();
		}

		if (this.keyStrokeDef == null) {
			ensureKeyStrokeDefinitionLoaded();
		}

		// request refresh at the layout
		getParent().requestRefresh(this);
	}

	/**
	 * Ensures that the internal keystroke definitions are loaded properly.
	 */
	private void ensureKeyStrokeDefinitionLoaded() {
		if (keyStrokeDef == null) {
			keyStrokeDef = getEditpart(getContext(),getModel().getKeyStrokeDefinition());
		}

		if (keyStrokeDef == null) {
			keyStrokeDef = getEditpart(getContext(),getModel().getTempStrokeDefinition());
		}
	}

	/**
	 * Returns the keyStrokeDefinition editpart.
	 * 
	 * @return the keyStrokeDef
	 */
	public IKeyStrokeDefinitionEditpart getKeyStrokeDefinition() {
		ensureKeyStrokeDefinitionLoaded();
		return keyStrokeDef;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION:
			internalSetKeyStrokeDefinition((YKeyStrokeDefinition) notification
					.getNewValue());
			break;
		case YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION:
			// if there is not defined keystroke definition, then use the temp
			// one
			if (getModel().getKeyStrokeDefinition() == null) {
				internalSetKeyStrokeDefinition((YKeyStrokeDefinition) notification
						.getNewValue());
			}
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.editparts.IInternalFocusingEditpart#activate()
	 */
	@Override
	public void activate() {
		viewContext = getView().getContext();
	}

	/**
	 * ONLY FOR TESTS!.
	 *
	 * @return the strategy
	 */
	public IFocusingStrategy getStrategy() {
		return strategy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			strategy = null;
		} finally {
			super.internalDispose();
		}
	}

}
