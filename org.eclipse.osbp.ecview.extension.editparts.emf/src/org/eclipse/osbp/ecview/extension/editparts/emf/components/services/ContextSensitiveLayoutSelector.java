/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.editparts.emf.components.services;

import org.eclipse.osbp.ecview.extension.api.IContextSensitiveLayoutSelector;
import org.eclipse.osbp.ui.api.functionlibrary.IFunctionLibraryService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

@Component
public class ContextSensitiveLayoutSelector implements
		IContextSensitiveLayoutSelector {

	private IFunctionLibraryService service;

	@Override
	public String selectComponent(Object dto) {
		return (String) service.invoke(FUNCTION_GROUP, FUNCTION_NAME, dto);
	}

	@Reference(cardinality=ReferenceCardinality.MANDATORY)
	void bind(IFunctionLibraryService service) {
		this.service = service;
	}
	
	void unbind(IFunctionLibraryService service) {
		this.service = null;
	}
	
}
