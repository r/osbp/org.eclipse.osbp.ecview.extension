/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.editparts.emf;

import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.extension.grid.CxGridCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.editparts.DelegatingGridCellStyleGeneratorFactory;
import org.eclipse.osbp.ecview.extension.grid.editparts.IGridDelegateCellStyleGeneratorEditpart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The implementation of the IOpenDialogCommandEditpart.
 */
public class GridDelegateCellStyleGeneratorEditpart extends
		ElementEditpart<CxGridCellStyleGenerator> implements
		IGridDelegateCellStyleGeneratorEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GridDelegateCellStyleGeneratorEditpart.class);
	
	/** The delegate. */
	private Object delegate;

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#createModel()
	 */
	@Override
	public Object getDelegate() {
		checkDisposed();

		if (delegate == null) {
			delegate = DelegatingGridCellStyleGeneratorFactory.getInstance()
					.createCellStyleGenerator(getViewContext(getModel()), this);

			LOGGER.info("Created cellStyleGenerator for " + getModel());
		}

		return delegate;
	}
}
