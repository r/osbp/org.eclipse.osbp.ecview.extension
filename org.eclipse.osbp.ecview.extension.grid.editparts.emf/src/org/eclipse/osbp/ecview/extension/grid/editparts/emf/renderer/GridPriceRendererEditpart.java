/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.DelegatingGridRendererFactory;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.IGridPriceRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;

/**
 * The Class GridPriceRendererEditpart.
 */
public class GridPriceRendererEditpart extends
		GridRendererEditpart<CxGridPriceRenderer>  implements IGridPriceRendererEditpart {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridRendererEditpart#internalCreateDelegate()
	 */
	@Override
	protected Object internalCreateDelegate() {
		return DelegatingGridRendererFactory.getInstance().createRenderer(
				getViewContext(getModel()), this);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelSet(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		switch (featureId) {
		case CxGridRendererPackage.CX_GRID_PRICE_RENDERER__NULL_REPRESENTATION:
		case CxGridRendererPackage.CX_GRID_PRICE_RENDERER__VALUE_PROPERTY_PATH:
		case CxGridRendererPackage.CX_GRID_PRICE_RENDERER__CURRENCY_PROPERTY_PATH:
			markDirty();
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}
}
