/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.editparts.emf;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;

/**
 * This class automatically connects to all EObjects in the containment chain as
 * long as the EClass is part of the CxGridPackage. <br>
 * Editparts are automatically added and removed from the EObjects.
 */
public class GridEditpartController extends EContentAdapter {

	private final IViewContext context;

	public GridEditpartController(IViewContext context) {
		this.context = context;
	}

	/**
	 * Add this adapter only to EObjects from grid package.
	 *
	 * @param notifier
	 *            the notifier
	 */
	protected void addAdapter(Notifier notifier) {
		for (Adapter adapter : notifier.eAdapters()) {
			if (adapter == this) {
				return;
			}
		}

		if (notifier instanceof EObject) {
			EObject e = (EObject) notifier;

			if (isProperPackage(e)) {
				notifier.eAdapters().add(this);
				initEditpart(notifier);
			}
		}
	}

	/**
	 * Checks if is proper package.
	 *
	 * @param e
	 *            the e
	 * @return true, if is proper package
	 */
	protected boolean isProperPackage(EObject e) {
		String nsURI = e.eClass().getEPackage().getNsURI();
		return nsURI.equals(CxGridPackage.eNS_URI)
				|| nsURI.equals(CxGridRendererPackage.eNS_URI);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.util.EContentAdapter#removeAdapter(org.eclipse.emf.common.notify.Notifier)
	 */
	@Override
	protected void removeAdapter(Notifier notifier) {
		super.removeAdapter(notifier);

		disposeEditpart(notifier);
	}

	/**
	 * Initializes the editpart.
	 *
	 * @param notifier
	 *            the notifier
	 */
	protected void initEditpart(Notifier notifier) {
		if (notifier instanceof YElement) {
			ElementEditpart.getEditpart(context, (YElement) notifier);
		}
	}

	/**
	 * Disposes the editpart if available.
	 *
	 * @param notifier
	 *            the notifier
	 */
	protected void disposeEditpart(Notifier notifier) {
		if (notifier instanceof YElement) {
			IElementEditpart ep = ElementEditpart
					.findEditPartFor((YElement) notifier);
			if (ep != null) {
				ep.requestDispose();
			}
		}
	}
}
