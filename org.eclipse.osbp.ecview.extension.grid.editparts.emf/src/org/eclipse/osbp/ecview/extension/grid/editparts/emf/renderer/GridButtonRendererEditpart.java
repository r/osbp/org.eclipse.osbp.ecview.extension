/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.DelegatingGridRendererFactory;
import org.eclipse.osbp.ecview.extension.grid.editparts.renderer.IGridButtonRendererEditpart;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;

/**
 * The Class GridButtonRendererEditpart.
 */
public class GridButtonRendererEditpart extends
		GridRendererEditpart<CxGridDateRenderer> implements IGridButtonRendererEditpart {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.extension.grid.editparts.emf.renderer.GridRendererEditpart#internalCreateDelegate()
	 */
	@Override
	protected Object internalCreateDelegate() {
		return DelegatingGridRendererFactory.getInstance().createRenderer(
				getViewContext(getModel()), this);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelSet(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		switch (featureId) {
		case CxGridRendererPackage.CX_GRID_BUTTON_RENDERER__NULL_REPRESENTATION:
			markDirty();
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}
}
