package org.eclipse.osbp.runtime.web.sample.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.runtime.web.sample.dtos.PersonDto;
import org.eclipse.osbp.runtime.web.sample.entities.Person;

@SuppressWarnings("all")
public class PersonDtoService extends AbstractDTOServiceWithMutablePersistence<PersonDto, Person> {
  public PersonDtoService() {
    // set the default persistence ID
    setPersistenceId("sample");
  }
  
  public Class<PersonDto> getDtoClass() {
    return PersonDto.class;
  }
  
  public Class<Person> getEntityClass() {
    return Person.class;
  }
  
  public Object getId(final PersonDto dto) {
    return dto.getUuid();
  }
}
