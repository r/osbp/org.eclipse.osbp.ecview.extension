package org.eclipse.osbp.runtime.web.sample.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;
import org.eclipse.osbp.runtime.web.sample.dtos.BaseUUIDDto;
import org.eclipse.osbp.runtime.web.sample.entities.BaseUUID;

@SuppressWarnings("all")
public class BaseUUIDDtoService extends AbstractDTOService<BaseUUIDDto, BaseUUID> {
  public Class<BaseUUIDDto> getDtoClass() {
    return BaseUUIDDto.class;
  }
  
  public Class<BaseUUID> getEntityClass() {
    return BaseUUID.class;
  }
  
  public Object getId(final BaseUUIDDto dto) {
    return dto.getUuid();
  }
}
