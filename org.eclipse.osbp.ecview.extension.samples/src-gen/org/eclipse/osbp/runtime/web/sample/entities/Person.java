package org.eclipse.osbp.runtime.web.sample.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.Filter;
import org.eclipse.osbp.runtime.common.annotations.Range;
import org.eclipse.osbp.runtime.web.sample.entities.Address;
import org.eclipse.osbp.runtime.web.sample.entities.BaseUUID;
import org.eclipse.osbp.runtime.web.sample.entities.Gender;

@Entity
@Table(name = "PERSON")
@SuppressWarnings("all")
public class Person extends BaseUUID implements IEntity {
  @Column(name = "FIRSTNAME")
  @Filter
  private String firstname;
  
  @Column(name = "LASTNAME")
  @Range
  private String lastname;
  
  @Column(name = "BIRTHDATE")
  @Temporal(value = TemporalType.DATE)
  @Filter
  @Valid
  private Date birthdate;
  
  @Column(name = "AGE")
  @Filter
  private int age;
  
  @Column(name = "WEIGHT")
  @Range
  private double weight;
  
  @Column(name = "GENDER")
  private Gender gender;
  
  @ManyToOne
  @JoinColumn(name = "ADDRESS_ID")
  private Address address;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the firstname property or <code>null</code> if not present.
   */
  public String getFirstname() {
    checkDisposed();
    return this.firstname;
  }
  
  /**
   * Sets the firstname property to this instance.
   */
  public void setFirstname(final String firstname) {
    checkDisposed();
    this.firstname = firstname;
  }
  
  /**
   * @return Returns the lastname property or <code>null</code> if not present.
   */
  public String getLastname() {
    checkDisposed();
    return this.lastname;
  }
  
  /**
   * Sets the lastname property to this instance.
   */
  public void setLastname(final String lastname) {
    checkDisposed();
    this.lastname = lastname;
  }
  
  /**
   * @return Returns the birthdate property or <code>null</code> if not present.
   */
  public Date getBirthdate() {
    checkDisposed();
    return this.birthdate;
  }
  
  /**
   * Sets the birthdate property to this instance.
   */
  public void setBirthdate(final Date birthdate) {
    checkDisposed();
    this.birthdate = birthdate;
  }
  
  /**
   * @return Returns the age property or <code>null</code> if not present.
   */
  public int getAge() {
    checkDisposed();
    return this.age;
  }
  
  /**
   * Sets the age property to this instance.
   */
  public void setAge(final int age) {
    checkDisposed();
    this.age = age;
  }
  
  /**
   * @return Returns the weight property or <code>null</code> if not present.
   */
  public double getWeight() {
    checkDisposed();
    return this.weight;
  }
  
  /**
   * Sets the weight property to this instance.
   */
  public void setWeight(final double weight) {
    checkDisposed();
    this.weight = weight;
  }
  
  /**
   * @return Returns the gender property or <code>null</code> if not present.
   */
  public Gender getGender() {
    checkDisposed();
    return this.gender;
  }
  
  /**
   * Sets the gender property to this instance.
   */
  public void setGender(final Gender gender) {
    checkDisposed();
    this.gender = gender;
  }
  
  /**
   * @return Returns the address property or <code>null</code> if not present.
   */
  public Address getAddress() {
    checkDisposed();
    return this.address;
  }
  
  /**
   * Sets the address property to this instance.
   */
  public void setAddress(final Address address) {
    checkDisposed();
    this.address = address;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
