package org.eclipse.osbp.runtime.web.sample.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.Filter;
import org.eclipse.osbp.runtime.web.sample.entities.BaseUUID;

@Entity
@Table(name = "COUNTRY")
@SuppressWarnings("all")
public class Country extends BaseUUID implements IEntity {
  @Column(name = "NAME")
  @Filter
  private String name;
  
  @Column(name = "ISO_CODE")
  @Filter
  private String isoCode;
  
  @Column(name = "EU_MEMBER")
  @Filter
  private boolean euMember;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    checkDisposed();
    return this.name;
  }
  
  /**
   * Sets the name property to this instance.
   */
  public void setName(final String name) {
    checkDisposed();
    this.name = name;
  }
  
  /**
   * @return Returns the isoCode property or <code>null</code> if not present.
   */
  public String getIsoCode() {
    checkDisposed();
    return this.isoCode;
  }
  
  /**
   * Sets the isoCode property to this instance.
   */
  public void setIsoCode(final String isoCode) {
    checkDisposed();
    this.isoCode = isoCode;
  }
  
  /**
   * @return Returns the euMember property or <code>null</code> if not present.
   */
  public boolean getEuMember() {
    checkDisposed();
    return this.euMember;
  }
  
  /**
   * Sets the euMember property to this instance.
   */
  public void setEuMember(final boolean euMember) {
    checkDisposed();
    this.euMember = euMember;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
