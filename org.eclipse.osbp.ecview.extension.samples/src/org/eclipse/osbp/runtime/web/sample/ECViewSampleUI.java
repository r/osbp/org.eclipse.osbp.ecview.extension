/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.runtime.web.sample;

import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.FilteringComponentEmbeddable;
import org.eclipse.osbp.runtime.web.sample.dtos.PersonDto;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
@Theme(ValoTheme.THEME_NAME)
public class ECViewSampleUI extends UI {

	private CssLayout content = new CssLayout();

	@Override
	protected void init(VaadinRequest request) {
		setContent(content);
		content.setSizeFull();

		FilteringComponentEmbeddable<PersonDto> comp = new FilteringComponentEmbeddable<>(PersonDto.class, 3);
		comp.setSizeFull();
		content.addComponent(comp);

		comp.init(getLocale());

		comp.setAcceptCallback(p -> {
			System.out.println(p);
		});

	}
}
