/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.presentation.converter;

import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.services.IServiceRegistry;
import org.eclipse.osbp.runtime.common.i18n.I18nUtil;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.web.vaadin.common.resource.IResourceProvider;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.server.Resource;

import org.eclipse.osbp.ecview.extension.model.converter.CxStringToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.CxStringToResourceConverter;

@SuppressWarnings("serial")
public class StringToResourceConverter implements Converter<Resource, String> {

	private IServiceRegistry registry;
	private CxStringToResourceConverter cxConverter;

	public StringToResourceConverter(IServiceRegistry registry,
			CxStringToResourceConverter cxConverter) {
		this.registry = registry;
		this.cxConverter = cxConverter;
	}

	@Override
	public String convertToModel(Resource value,
			Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		throw new com.vaadin.data.util.converter.Converter.ConversionException(
				"StringToResourceConverter is readonly!");
	}

	@Override
	public Resource convertToPresentation(String value,
			Class<? extends Resource> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {

		IResourceProvider provider = registry
				.getService(IResourceProvider.class.getName());

		for (CxStringToResourceConfig config : cxConverter.getConfigs()) {
			if (isMatch(value, config)) {

				String iconKey = I18nUtil.getImageKey(config
						.getResourceThemePath());

				if (isValid(iconKey)) {
					if (provider != null) {
						return provider.getResource(iconKey);
					}
				}

				throw new IllegalStateException("No image defined for "
						+ config);
			}
		}

		return null;
	}

	private boolean isValid(String iconKey) {
		return iconKey != null && !iconKey.equals("");
	}

	protected boolean isMatch(String value, CxStringToResourceConfig config) {
		String ref = config.getValue();

		switch (config.getCompare()) {
		case EQUAL:
			return ref.equals(value);
		case GREATER_EQUAL:
			return ref.compareTo(value) >= 0;
		case GREATER_THAN:
			return ref.compareTo(value) > 0;
		case LOWER_EQUAL:
			return ref.compareTo(value) <= 0;
		case LOWER_THAN:
			return ref.compareTo(value) < 0;
		case NOT_EQUAL:
			return ref.compareTo(value) != 0;
		}
		return false;
	}

	@Override
	public Class<String> getModelType() {
		return String.class;
	}

	@Override
	public Class<Resource> getPresentationType() {
		return Resource.class;
	}

}
