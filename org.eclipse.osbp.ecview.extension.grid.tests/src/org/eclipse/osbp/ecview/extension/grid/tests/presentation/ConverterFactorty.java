/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.grid.tests.presentation;

import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YDelegateConverter;
import org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory;
import org.osgi.service.component.annotations.Component;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;

@Component(immediate = true)
public class ConverterFactorty implements IConverterFactory {

	@Override
	public boolean isFor(IViewContext uiContext, IConverterEditpart editpart) {
		return true;
	}

	@Override
	public Object createConverter(IViewContext uiContext,
			IConverterEditpart editpart) throws IllegalArgumentException {
		YDelegateConverter yConverter = (YDelegateConverter) editpart
				.getModel();
		if (yConverter.getConverterId().equals("genderToImageConverter")) {
			return new GenderToImageConverter();
		}

		return null;
	}

	@SuppressWarnings("serial")
	public static class GenderToImageConverter implements
			Converter<Resource, Gender> {

		@Override
		public Gender convertToModel(Resource value,
				Class<? extends Gender> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			return null;
		}

		@Override
		public Resource convertToPresentation(Gender value,
				Class<? extends Resource> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {

			switch (value) {
			case FEMALE:
				return new ThemeResource("gridsample/female.gif");
			case MALE:
				return new ThemeResource("gridsample/male.gif");
			}
			return null;
		}

		@Override
		public Class<Gender> getModelType() {
			return Gender.class;
		}

		@Override
		public Class<Resource> getPresentationType() {
			return Resource.class;
		}

	}

}
