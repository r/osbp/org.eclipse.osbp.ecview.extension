/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.vaadin.components;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.converter.CustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.vaadin.components.utils.FunctionWrapper;
import org.eclipse.osbp.runtime.web.vaadin.components.fields.DecimalField;
import org.eclipse.osbp.ui.api.functionlibrary.IFunctionLibraryService;
import org.eclipse.osbp.utils.functionnormalizer.api.FunctionTypingAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A decimalfield specific price styling.
 */
@SuppressWarnings("serial")
public class CustomDecimalField extends DecimalField {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomDecimalField.class);

	/** The view context. */
	private final IViewContext viewContext;

	/** The converter. */
	private CustomDecimalConverter converter;

	/**
	 * Instantiates a new custom decimal field.
	 */
	public CustomDecimalField() {
		this(null, null);
	}

	/**
	 * Instantiates a new custom decimal field.
	 *
	 * @param caption
	 *            the caption
	 * @param viewContext
	 *            the view context
	 */
	public CustomDecimalField(String caption, IViewContext viewContext) {
		this(caption, null, viewContext);
	}

	/**
	 * Instantiates a new custom decimal field.
	 *
	 * @param caption
	 *            the caption
	 * @param converter
	 *            the converter
	 * @param viewContext
	 *            the view context
	 */
	public CustomDecimalField(String caption, CustomDecimalConverter converter, IViewContext viewContext) {
		super(caption, converter);

		this.viewContext = viewContext;

		setNullRepresentation("");
		setNullSettingAllowed(false);
	}

	/**
	 * Sets the converter.
	 *
	 * @param converter
	 *            the new converter
	 */
	public void setConverter(CustomDecimalConverter converter) {
		this.converter = converter != null ? converter : new CustomDecimalConverter();
		super.setConverter(this.converter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.web.vaadin.components.fields.DecimalField#
	 * setInternalValue(java.lang.String)
	 */
	protected void setInternalValue(String newValue) {
		super.setInternalValue(newValue);
		// TODO (JCD): Only temporary till Mr. Pirchner fixes the isModified
		// bug.
		getState().modified = true;
		// --------

		if (converter != null) {
			callFunctions(converter.getProperties());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.AbstractField#setConvertedValue(java.lang.Object)
	 */
	@Override
	public void setConvertedValue(Object value) {
		if (converter != null) {
			converter.setConvertedDecimalFormat(null);
		}

		boolean oldReadonly = isReadOnly();
		try {
			setReadOnly(false);
			super.setConvertedValue(value);
		} finally {
			setReadOnly(oldReadonly);
		}
	}

	/**
	 * Call functions.
	 *
	 * @param properties
	 *            the properties
	 */
	// ++++++++++++++++ Helper Methods ++++++++++++++++++++
	private void callFunctions(EMap<String, String> properties) {
		FunctionTypingAPI functionTypingAPI = new FunctionTypingAPI();
		for (Entry<String, String> entry : properties) {
			if (functionTypingAPI.getFunctionCssTypeName().equalsIgnoreCase(entry.getKey())) {
				handleCss(entry.getValue());
			}
			LOGGER.debug("entry.getKey(): " + entry.getKey());
			LOGGER.debug("entry.getValue(): " + entry.getValue());
		}

	}

	/**
	 * Handle css.
	 *
	 * @param functionFqn
	 *            the function fqn
	 */
	@SuppressWarnings("unchecked")
	private void handleCss(String functionFqn) {
		FunctionWrapper wrapper = new FunctionWrapper(functionFqn);
		Object result = null;
		try {
			if (getPropertyDataSource() == null || isBuffered() || isModified()) {
				String value = getInternalValue();
				try {
					result = converter.convertToModel(value, Double.class, getLocale());
				} catch (Exception e) {
					// nothing to do
				}
			} else {
				Object value = getPropertyDataSource().getValue();
				if (value != null) {
					result = (Double) value;
				}
			}

			if (result != null) {
				IFunctionLibraryService functionService = viewContext
						.getService(IFunctionLibraryService.class.getName());
				if (functionService == null) {
					LOGGER.error("No implementation of IFunctionLibraryService available.");
					return;
				}

				Map<String, Boolean> callResult = (Map<String, Boolean>) functionService
						.invoke(wrapper.getClassName(), wrapper.getMethodName(), result);
				if (callResult != null) {
					removeStyleNames(callResult);
					addStyleNames(callResult);
				}
			}
		} catch (ClassCastException ex) {
			// TODO (JCD): Translation
			LOGGER.error("Return type of the function call '" + functionFqn + "' is not a Map!");
		}
	}

	/**
	 * Removes the style names.
	 *
	 * @param styleNames
	 *            the style names
	 */
	private void removeStyleNames(Map<String, Boolean> styleNames) {
		for (Entry<String, Boolean> entry : styleNames.entrySet()) {
			removeStyleName(entry.getKey());
		}
	}

	/**
	 * Adds the style names.
	 *
	 * @param styleNames
	 *            the style names
	 */
	private void addStyleNames(Map<String, Boolean> styleNames) {
		for (Entry<String, Boolean> entry : styleNames.entrySet()) {
			if (entry.getValue()) {
				addStyleName(entry.getKey());
			}
		}
	}

}
