/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.components;

import java.util.Locale;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.EMFObservables;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableValueEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YValueBindable;
import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype;
import org.eclipse.osbp.ecview.core.util.emf.ModelUtil;
import org.eclipse.osbp.ecview.extension.editparts.components.ICustomDecimalFieldEditpart;
import org.eclipse.osbp.ecview.extension.model.YCustomDecimalField;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.converter.CustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.vaadin.components.CustomDecimalField;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.IBindingManager;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.internal.util.Util;
import org.eclipse.osbp.runtime.web.vaadin.components.converter.DecimalDoubleConverter;
import org.eclipse.osbp.runtime.web.vaadin.databinding.VaadinObservables;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.server.ErrorMessage;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Field;

/**
 * This presenter is responsible to render a text area on the given layout.
 */
public class CustomDecimalFieldPresentation extends
		AbstractFieldWidgetPresenter<Component> {

	/** The model access. */
	private final ModelAccess modelAccess;
	
	/** The custom decimal field. */
	private CustomField customDecimalField;
	
	/** The binding_value to ui. */
	private Binding binding_valueToUI;
	
	/** The property. */
	private ObjectProperty<Double> property;

	/**
	 * Constructor.
	 * 
	 * @param editpart
	 *            The editpart of that presenter
	 */
	public CustomDecimalFieldPresentation(IElementEditpart editpart) {
		super((ICustomDecimalFieldEditpart) editpart);
		this.modelAccess = new ModelAccess(
				(YCustomDecimalField) editpart.getModel());
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "serial", "unchecked" })
	@Override
	public Component doCreateWidget(Object parent) {
		if (customDecimalField == null) {

			customDecimalField = new CustomField(
					(CustomDecimalConverter) getConverter());
			customDecimalField.addStyleName(CSS_CLASS_CONTROL);
			customDecimalField.setImmediate(true);
			setupComponent(customDecimalField, getCastedModel());

			associateWidget(customDecimalField, modelAccess.yField);
			if (modelAccess.isCssIdValid()) {
				customDecimalField.setId(modelAccess.getCssID());
			} else {
				customDecimalField.setId(getEditpart().getId());
			}

			IViewContext context = getViewContext();
			customDecimalField.setLocale(context.getLocale());
			property = new ObjectProperty<Double>(0d, Double.class);
			customDecimalField.setPropertyDataSource(property);

			customDecimalField
					.addValueChangeListener(new Property.ValueChangeListener() {
						@Override
						public void valueChange(ValueChangeEvent event) {
							if (binding_valueToUI != null) {
								updateUiToECViewModel();
							}
						}
					});

			if (modelAccess.isCssClassValid()) {
				customDecimalField.addStyleName(modelAccess.getCssClass());
			}

			applyCaptions();
			doApplyDatatype(modelAccess.yField.getDatatype());

			initializeField(customDecimalField);

			// creates the binding for the field
			createBindings(modelAccess.yField, customDecimalField);

			// send an event, that the content was rendered again
			sendRenderedLifecycleEvent();
		}
		return customDecimalField;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter#getDefaultConverter()
	 */
	@Override
	protected Converter<?, ?> getDefaultConverter() {
		return new DecimalDoubleConverter();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter#doUpdateConverter(com.vaadin.data.util.converter.Converter)
	 */
	@SuppressWarnings("rawtypes")
	protected void doUpdateConverter(Converter converter) {
		super.doUpdateConverter(converter);

		// if converter changes, then apply the settings from datatype to it
		doApplyDatatype(modelAccess.yField.getDatatype());
	}

	/**
	 * Applies the datatype options to the field.
	 *
	 * @param yDt
	 *            the y dt
	 */
	protected void doApplyDatatype(YDatatype yDt) {
		if (customDecimalField == null) {
			return;
		}

		int oldPrecision = customDecimalField.getPrecision();
		if (yDt == null) {
			customDecimalField.setPrecision(2);
			customDecimalField.setUseGrouping(true);
			customDecimalField.setMarkNegative(true);
		} else {
			YDecimalDatatype yCasted = (YDecimalDatatype) yDt;
			customDecimalField.setPrecision(yCasted.getPrecision());
			customDecimalField.setUseGrouping(yCasted.isGrouping());
			customDecimalField.setMarkNegative(yCasted.isMarkNegative());
		}

		if (isRendered()) {
			// if the precision changed, then update the value from the ui field
			// to the ECViewModel
			if (oldPrecision != customDecimalField.getPrecision()) {
				updateUiToECViewModel();
			}
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractVaadinWidgetPresenter#doUpdateLocale(java.util.Locale)
	 */
	@Override
	protected void doUpdateLocale(Locale locale) {
		// update the captions
		applyCaptions();

		// tell the number field about the locale change
		customDecimalField.setLocale(locale);
	}

	/**
	 * Applies the labels to the widgets.
	 */
	protected void applyCaptions() {
		Util.applyCaptions(getI18nService(), modelAccess.getLabel(),
				modelAccess.getLabelI18nKey(), getLocale(), customDecimalField);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractFieldWidgetPresenter#doGetField()
	 */
	@Override
	protected Field<?> doGetField() {
		return customDecimalField;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.common.AbstractVaadinWidgetPresenter#internalGetObservableEndpoint(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint)
	 */
	@Override
	protected IObservable internalGetObservableEndpoint(
			YEmbeddableBindingEndpoint bindableValue) {
		if (bindableValue == null) {
			throw new IllegalArgumentException(
					"BindableValue must not be null!");
		}

		if (bindableValue instanceof YEmbeddableValueEndpoint) {
			return internalGetValueEndpoint();
		}
		throw new IllegalArgumentException("Not a valid input: "
				+ bindableValue);
	}

	/**
	 * Returns the observable to observe value.
	 *
	 * @return the i observable value
	 */
	protected IObservableValue internalGetValueEndpoint() {
		// return the observable value for text
		return EMFObservables.observeValue(castEObject(getModel()),
				YECviewPackage.Literals.YCUSTOM_DECIMAL_FIELD__VALUE);
	}

	/**
	 * Creates the bindings for the given values.
	 *
	 * @param yField
	 *            the y field
	 * @param field
	 *            the field
	 */
	protected void createBindings(YCustomDecimalField yField,
			CustomDecimalField field) {
		// create the model binding from ridget to ECView-model

		binding_valueToUI = createModelBinding(castEObject(getModel()),
				YECviewPackage.Literals.YCUSTOM_DECIMAL_FIELD__VALUE, field,
				null, null);

		registerBinding(binding_valueToUI);

		super.createBindings(yField, field, null);
	}

	/**
	 * Creates the model binding.
	 *
	 * @param model
	 *            the model
	 * @param modelFeature
	 *            the model feature
	 * @param field
	 *            the field
	 * @param targetToModel
	 *            the target to model
	 * @param modelToTarget
	 *            the model to target
	 * @return the binding
	 */
	protected Binding createModelBinding(EObject model,
			EStructuralFeature modelFeature, AbstractField<?> field,
			UpdateValueStrategy targetToModel, UpdateValueStrategy modelToTarget) {
		IBindingManager bindingManager = getViewContext()
				.getService(
						org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager.class
								.getName());
		if (bindingManager != null) {
			// bind the value of yText to textRidget
			IObservableValue modelObservable = EMFObservables.observeValue(
					model, modelFeature);
			IObservableValue uiObservable = VaadinObservables
					.observeConvertedValue(field);
			return bindingManager.bindValue(uiObservable, modelObservable,
					targetToModel, modelToTarget);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#getWidget()
	 */
	@Override
	public Component getWidget() {
		return customDecimalField;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation#isRendered()
	 */
	@Override
	public boolean isRendered() {
		return customDecimalField != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doUnrender() {
		if (customDecimalField != null) {

			// unbind all active bindings
			unbind();

			Component parent = ((Component) customDecimalField.getParent());
			if (parent != null && parent instanceof ComponentContainer) {
				((ComponentContainer) parent)
						.removeComponent(customDecimalField);
			}

			// remove assocations
			unassociateWidget(customDecimalField);

			customDecimalField = null;

			sendUnrenderedLifecycleEvent();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			unrender();
			binding_valueToUI = null;
		} finally {
			super.internalDispose();
		}
	}

	/**
	 * Update ui to ec view model.
	 */
	protected void updateUiToECViewModel() {
		if (binding_valueToUI != null) {
			binding_valueToUI.updateTargetToModel();
		}

		Binding domainToEObjectBinding = ModelUtil
				.getValueBinding((YValueBindable) getModel());
		if (domainToEObjectBinding != null) {
			domainToEObjectBinding.updateTargetToModel();
		}
	}

	/**
	 * A helper class.
	 */
	private static class ModelAccess {
		
		/** The y field. */
		private final YCustomDecimalField yField;

		/**
		 * Instantiates a new model access.
		 *
		 * @param yField
		 *            the y field
		 */
		public ModelAccess(YCustomDecimalField yField) {
			super();
			this.yField = yField;
		}

		/**
		 * Gets the css class.
		 *
		 * @return the css class
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssClass()
		 */
		public String getCssClass() {
			return yField.getCssClass();
		}

		/**
		 * Returns true, if the css class is not null and not empty.
		 *
		 * @return true, if is css class valid
		 */
		public boolean isCssClassValid() {
			return getCssClass() != null && !getCssClass().equals("");
		}

		/**
		 * Gets the css id.
		 *
		 * @return the css id
		 * @see org.eclipse.osbp.ecview.core.ui.core.model.core.YCssAble#getCssID()
		 */
		public String getCssID() {
			return yField.getCssID();
		}

		/**
		 * Returns true, if the css id is not null and not empty.
		 *
		 * @return true, if is css id valid
		 */
		public boolean isCssIdValid() {
			return getCssID() != null && !getCssID().equals("");
		}

		/**
		 * Returns the label.
		 *
		 * @return the label
		 */
		public String getLabel() {
			return yField.getDatadescription() != null ? yField
					.getDatadescription().getLabel() : null;
		}

		/**
		 * Returns the label.
		 *
		 * @return the label i18n key
		 */
		public String getLabelI18nKey() {
			return yField.getDatadescription() != null ? yField
					.getDatadescription().getLabelI18nKey() : null;
		}
	}

	/**
	 * The Class CustomField.
	 */
	@SuppressWarnings("serial")
	private class CustomField extends CustomDecimalField {

		/**
		 * Instantiates a new custom field.
		 *
		 * @param converter
		 *            the converter
		 */
		public CustomField(CustomDecimalConverter converter) {
			super("", getViewContext());
			setConverter(converter);
		}

		/* (non-Javadoc)
		 * @see com.vaadin.ui.AbstractField#getErrorMessage()
		 */
		@Override
		public ErrorMessage getErrorMessage() {
			if (isDisposed()) {
				// after disposal, Vaadin will call this method once.
				return null;
			}

			ErrorMessage message = super.getErrorMessage();
			reportValidationError(message);
			return message;
		}
	}
}
