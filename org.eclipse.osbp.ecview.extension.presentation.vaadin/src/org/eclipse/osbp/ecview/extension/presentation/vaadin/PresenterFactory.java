/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.presentation.IPresentationFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.extension.editparts.IContentSensitiveLayoutEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IMaskedDecimalFieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IMaskedNumericFieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IMaskedTextFieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IPrefixedMaskedTextFieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IRichTextAreaEditpart;
import org.eclipse.osbp.ecview.extension.editparts.IStrategyLayoutEditpart;
import org.eclipse.osbp.ecview.extension.editparts.components.IBlobUploadComponentEditpart;
import org.eclipse.osbp.ecview.extension.editparts.components.ICustomDecimalFieldEditpart;
import org.eclipse.osbp.ecview.extension.editparts.components.II18nComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.editparts.components.IIconComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.editparts.components.IPairComboBoxEditpart;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.BlobUploadComponentPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.ContentSensitiveLayoutPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.CustomDecimalFieldPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.I18nComboBoxPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.IconComboBoxPresentation;
//lunifera@80.156.28.28/osbpgit/org.eclipse.osbp.ecview.extension.git
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.MaskedDecimalFieldPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.MaskedNumericFieldPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.MaskedTextFieldPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.PairComboBoxPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.PrefixedMaskedTextFieldPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.components.RichTextAreaPresentation;
import org.eclipse.osbp.ecview.extension.presentation.vaadin.strategy.StrategyLayoutPresentation;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.osgi.service.component.annotations.Component;

/**
 * The presenter factory.
 */
@Component(immediate = true, service = { IPresentationFactory.class })
public class PresenterFactory implements IPresentationFactory {

	/**
	 * Instantiates a new presenter factory.
	 */
	public PresenterFactory() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.presentation.IPresentationFactory
	 * #isFor(org.eclipse.osbp.ecview.core.common.context.IViewContext,
	 * org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart)
	 */
	@Override
	public boolean isFor(IViewContext uiContext, IElementEditpart editpart) {
		String presentationURI = uiContext.getPresentationURI();
		return presentationURI != null
				&& presentationURI.equals(VaadinRenderer.UI_KIT_URI);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.presentation.IPresentationFactory
	 * #createPresentation
	 * (org.eclipse.osbp.ecview.core.common.context.IViewContext,
	 * org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends IWidgetPresentation<?>> A createPresentation(
			IViewContext uiContext, IElementEditpart editpart)
			throws IllegalArgumentException {
		if (editpart instanceof IStrategyLayoutEditpart) {
			return (A) new StrategyLayoutPresentation(editpart);
		} else if (editpart instanceof IBlobUploadComponentEditpart) {
			return (A) new BlobUploadComponentPresentation(editpart);
		} else if (editpart instanceof ICustomDecimalFieldEditpart) {
			return (A) new CustomDecimalFieldPresentation(editpart);
		} else if (editpart instanceof IIconComboBoxEditpart) {
			return (A) new IconComboBoxPresentation(editpart);
		} else if (editpart instanceof II18nComboBoxEditpart) {
			return (A) new I18nComboBoxPresentation(editpart);
		} else if (editpart instanceof IPairComboBoxEditpart) {
			return (A) new PairComboBoxPresentation(editpart);
		} else if (editpart instanceof IContentSensitiveLayoutEditpart) {
			return (A) new ContentSensitiveLayoutPresentation(editpart);
		} else if (editpart instanceof IRichTextAreaEditpart) {
			return (A) new RichTextAreaPresentation(editpart);
		} else if (editpart instanceof IMaskedTextFieldEditpart) {
			return (A) new MaskedTextFieldPresentation(editpart);
		} else if (editpart instanceof IPrefixedMaskedTextFieldEditpart) {
			return (A) new PrefixedMaskedTextFieldPresentation(editpart);
		} else if (editpart instanceof IMaskedDecimalFieldEditpart) {
			return (A) new MaskedDecimalFieldPresentation(editpart);
		} else if (editpart instanceof IMaskedNumericFieldEditpart) {
			return (A) new MaskedNumericFieldPresentation(editpart);
		}

		throw new IllegalArgumentException(String.format(
				"No presenter available for editpart %s[%s]", editpart
						.getClass().getName(), editpart.getId()));
	}
}
