package org.eclipse.osbp.ecview.extension.presentation.vaadin.converter;

import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToByteArrayConverter;
import org.eclipse.osbp.ui.api.customfields.IBlobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.util.converter.Converter;

public class BlobImageConverter implements Converter<byte[], String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7275925734059030659L;

	private static final Logger LOGGER = LoggerFactory.getLogger(BlobImageConverter.class);

	final IViewContext viewContext;
	final YStringToByteArrayConverter yConverter;

	public BlobImageConverter(IViewContext uiContext, YStringToByteArrayConverter yConverter) {
		this.viewContext = uiContext;
		this.yConverter = yConverter;
	}

	@Override
	public String convertToModel(byte[] value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		throw new UnsupportedOperationException("Not a valid call!");
	}

	@Override
	public byte[] convertToPresentation(String value, Class<? extends byte[]> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		IBlobService blobService = viewContext.getService(IBlobService.class.getName());
		return blobService.getByteArrayImage(value, 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<String> getModelType() {
		return String.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<byte[]> getPresentationType() {
		return byte[].class;
	}

}
