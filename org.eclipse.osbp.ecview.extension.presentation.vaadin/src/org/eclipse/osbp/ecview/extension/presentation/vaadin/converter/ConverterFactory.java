/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.converter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory;
import org.eclipse.osbp.ecview.extension.editparts.converter.ICustomDecimalConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.IDecimalToUomoConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.INumericToResourceConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.INumericToUomoConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.IObjectToStringConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.ISimpleDecimalConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.IStringToByteArrayConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.IStringToResourceConverterEditpart;
import org.eclipse.osbp.ecview.extension.editparts.converter.IVaaclipseUiThemeToStringConverterEditpart;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToByteArrayConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter;
import org.osgi.service.component.annotations.Component;

/**
 * A factory for creating Converter objects.
 */
@Component(immediate = true)
public class ConverterFactory implements IConverterFactory {

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory#isFor(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart)
	 */
	@Override
	public boolean isFor(IViewContext uiContext, IConverterEditpart editpart) {
		EObject model = (EObject) editpart.getModel();
		return model.eClass().getEPackage() == YConverterPackage.eINSTANCE;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory#createConverter(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart)
	 */
	@Override
	public Object createConverter(IViewContext uiContext,
			IConverterEditpart editpart) throws IllegalArgumentException {

		if (editpart instanceof IObjectToStringConverterEditpart) {
			return new ObjectToStringConverter();
		} else if (editpart instanceof IVaaclipseUiThemeToStringConverterEditpart) {
			return new VaaclipseUiThemeToStringConverter();
		} else if (editpart instanceof IStringToResourceConverterEditpart) {
			return new StringToResourceConverter(uiContext,
					(YStringToResourceConverter) editpart.getModel());
		} else if (editpart instanceof INumericToUomoConverterEditpart) {
			return new NumericToUomoConverter(uiContext,
					(YNumericToUomoConverter) editpart.getModel());
		} else if (editpart instanceof INumericToResourceConverterEditpart) {
			return new NumericToResourceConverter(uiContext,
					(YNumericToResourceConverter) editpart.getModel());
		} else if (editpart instanceof IDecimalToUomoConverterEditpart) {
			return new DecimalToUomoConverter(uiContext,
					(YDecimalToUomoConverter) editpart.getModel());
		} else if (editpart instanceof ICustomDecimalConverterEditpart) {
			return new CustomDecimalConverter(uiContext,
					(YCustomDecimalConverter) editpart.getModel());
		} else if (editpart instanceof ISimpleDecimalConverterEditpart) {
			return new SimpleDecimalConverter(uiContext,
					(YSimpleDecimalConverter) editpart.getModel());
		} else if (editpart instanceof IStringToByteArrayConverterEditpart) {
			return new BlobImageConverter(uiContext,
					(YStringToByteArrayConverter) editpart.getModel());
		}

		throw new IllegalArgumentException("Not a valid editpart: "
				+ editpart.getClass().getName());
	}

}
