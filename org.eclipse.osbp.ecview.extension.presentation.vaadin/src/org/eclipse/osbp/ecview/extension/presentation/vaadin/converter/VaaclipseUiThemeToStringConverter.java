/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.presentation.vaadin.converter;

import java.util.Locale;

import org.eclipse.osbp.utils.themes.ui.VaaclipseUiTheme;

import com.vaadin.data.util.converter.Converter;

/**
 * The Class VaaclipseUiThemeToStringConverter.
 */
@SuppressWarnings("serial")
public class VaaclipseUiThemeToStringConverter implements Converter<VaaclipseUiTheme, String> {

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#convertToModel(java.lang.Object, java.lang.Class, java.util.Locale)
	 */
	@Override
	public String convertToModel(VaaclipseUiTheme value,
			Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value!=null ? value.getId() : "";
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#convertToPresentation(java.lang.Object, java.lang.Class, java.util.Locale)
	 */
	@Override
	public VaaclipseUiTheme convertToPresentation(String value,
			Class<? extends VaaclipseUiTheme> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return VaaclipseUiTheme.forText(value);
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#getModelType()
	 */
	@Override
	public Class<VaaclipseUiTheme> getPresentationType() {
		return VaaclipseUiTheme.class;
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.util.converter.Converter#getPresentationType()
	 */
	@Override
	public Class<String> getModelType() {
		return String.class;
	}

}
