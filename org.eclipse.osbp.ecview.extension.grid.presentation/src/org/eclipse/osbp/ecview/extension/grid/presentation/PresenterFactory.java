/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.presentation;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.presentation.IPresentationFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.runtime.web.ecview.presentation.vaadin.VaadinRenderer;
import org.osgi.service.component.annotations.Component;

import org.eclipse.osbp.ecview.extension.grid.editparts.IGridEditpart;

/**
 * The presenter factory.
 */
@Component(immediate = true, service = { IPresentationFactory.class })
public class PresenterFactory implements IPresentationFactory {

	/**
	 * Instantiates a new presenter factory.
	 */
	public PresenterFactory() {

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IPresentationFactory#isFor(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart)
	 */
	@Override
	public boolean isFor(IViewContext uiContext, IElementEditpart editpart) {
		String presentationURI = uiContext.getPresentationURI();
		return presentationURI != null
				&& presentationURI.equals(VaadinRenderer.UI_KIT_URI);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.presentation.IPresentationFactory#createPresentation(org.eclipse.osbp.ecview.core.common.context.IViewContext, org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends IWidgetPresentation<?>> A createPresentation(
			IViewContext uiContext, IElementEditpart editpart)
			throws IllegalArgumentException {
		if (editpart instanceof IGridEditpart) {
			return (A) new GridPresentation(editpart);
		}

		throw new IllegalArgumentException(String.format(
				"No presenter available for editpart %s[%s]", editpart
						.getClass().getName(), editpart.getId()));
	}
}
