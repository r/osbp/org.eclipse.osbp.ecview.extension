/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.presentation.converter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.presentation.IConverterFactory;
import org.eclipse.osbp.ecview.extension.grid.editparts.converter.IGridNestedPropertyConverterEditpart;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;
import org.osgi.service.component.annotations.Component;

/**
 * A factory for creating Converter objects.
 */
@Component(immediate = true)
public class ConverterFactory implements IConverterFactory {

	@Override
	public boolean isFor(IViewContext uiContext, IConverterEditpart editpart) {
		EObject model = (EObject) editpart.getModel();
		return model.eClass().getEPackage() == CxGridRendererPackage.eINSTANCE;
	}

	@Override
	public Object createConverter(IViewContext uiContext, IConverterEditpart editpart) throws IllegalArgumentException {

		if (editpart instanceof IGridNestedPropertyConverterEditpart) {
			return new GridNestedPropertyConverter((IGridNestedPropertyConverterEditpart) editpart);
		}

		throw new IllegalArgumentException("Not a valid editpart: " + editpart.getClass().getName());
	}

}
